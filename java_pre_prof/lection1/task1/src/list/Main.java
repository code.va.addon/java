/* Вывод на экран элементов List:
Создать список, заполнить его на 10 элементов и вывести на экран содержимое используя iterator. */
package list;

import java.util.ArrayList;
import java.util.Iterator;

class Main {
    public static void main(String[] args) {
        ArrayList<Integer> myList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            myList.add(i + 1);
        }

        Iterator<Integer> iter = myList.iterator();

        while(iter.hasNext()) {
            System.out.println(iter.next());
        }
    }
}