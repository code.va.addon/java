/* Дописать логику, чтобы метод compareTo() осуществил поиск по скорости(если же скорость у 2-х объектов
равна, то ищем по цене) -> цене - > моделе -> цвету машины. */
package car;

import java.util.Arrays;

class Main {
    public static void main(String[] args) {
        Car porshe = new Car("porshe", "red", 200, 300, 100_000f);
        Car mersedes = new Car("mersedes", "black", 500, 200, 150_000f);
        Car reno = new Car("reno", "black", 350, 200, 90_000f);
        Car[] cars = {porshe, mersedes, reno};

        Arrays.sort(cars);

        for (Car car : cars) {
            System.out.println(car);
        }
    }
}
