package car;

class Car implements Comparable<Car> {
    private String color, model;
    private int weight, speed;
    private float price;

    Car(String model, String color, int weight, int speed, float price) {
        this.model = model;
        this.color = color;
        this.weight = weight;
        this.speed = speed;
        this.price = price;
    }

    @Override
    public String toString() {
        String separation = "";

        for (int i = 0; i < 20; i++) {
            separation += "=";
        }
        separation += "\n";
        return separation + String.format(
            "model: %1$s;\ncolor: %2$s;\nweight: %3$d;\nspeed: %4$d\nprice: %5$f\n", 
            model,
            color,
            weight,
            speed,
            price
        ) + separation;
    }

    private int compr(float num) {
        int result = 0;

        if (num > 0) ++result;
        else if (num < 0) --result;
        return result;
    }

    private int compareToColor(Car obj) {
        return color.compareTo(obj.color);
    }

    private int compareToModel(Car obj) {
        int result = model.compareTo(obj.model);
        if (result == 0) result = compareToColor(obj);
        return result;
    }

    private int compareToPrice(Car obj) {
        int result = compr(price - obj.price);
        if (result == 0) result = compareToModel(obj);
        return result;
    }

    @Override
    public int compareTo(Car obj) {
        Car car = (Car) obj;
        int result = compr(speed - car.speed);
        if (result == 0) result = compareToPrice(car);
        return result;
    }
}
