package hashtable;

import java.util.Hashtable;
import java.util.Scanner;

public class SimpleHashtable extends Hashtable<String, String> {
    private static final Scanner INPUT;

    static {
        INPUT = new Scanner(System.in);
    }
    public static void main(String[] args) {
        SimpleHashtable table = new SimpleHashtable();

        table.put();
        table.put();
        System.out.println(table);
        table.replace();
        System.out.println(table);
        table.remove();
        table.remove();
        System.out.println(table);
    }

    private String prompt(String message) {
        System.out.print(message);
        return INPUT.next();
    }

    void put() {
        String city = prompt("enter please city: ");
        String family = prompt("enter please family: ");
        put(city, family);
    }

    void remove() {
        String city = prompt("enter please city for remove: ");
        String family = prompt("enter please family for remove or \"null\": ");
        boolean result = (family.equals("null")) ? remove(city) != null : remove(city, family);

        if (result) System.out.println("delete object");
        else System.out.println("not delete object");
    }

    void replace() {
        String city = prompt("enter please city for replace: ");
        String newFamily = prompt("enter please new family for replace: ");
        String oldFamily = prompt("enter please old family for replace or \"null\": ");
        boolean result = (oldFamily.equals("null")) ? replace(city, newFamily) != null : replace(city, newFamily, oldFamily);

        if (result) System.out.println("replace object");
        else System.out.println("not replace object");
    }
}
