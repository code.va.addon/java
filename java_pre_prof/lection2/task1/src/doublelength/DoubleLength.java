/* Используя коллекцию удвойте слово:
1. Введите с клавиатуры 5 слов в список строк.
2. Метод doubleValues должен удваивать слова по принципу a,b,c -> a,a,b,b,c,c.
3. Используя цикл for выведите результат на экран, каждое значение с новой строки. */
package doublelength;

import java.util.ArrayList;
import java.util.Scanner;

class DoubleLength extends ArrayList<String> {
    private final static Scanner INPUT;
    private int capacity;
    public static void main(String[] args) {
        DoubleLength words = new DoubleLength(5);
        System.out.println(words.add());

        for (int i = 0; i < words.size(); i++) {
            System.out.println(words.get(i));
        }
    }

    static {
        INPUT = new Scanner(System.in);
    }

    DoubleLength(int capacity) {
        super(capacity);
        this.capacity = capacity;
    }

    private String doubleValue() {
        String element = INPUT.next();
        String result = "";

        for (int i = 0; i < element.length(); i++) {
            String charElement = String.valueOf(element.charAt(i));
            result += charElement + charElement;
        }
        return result;
    }

    boolean add() {
        boolean result = true;

        for (int i = 0; i < capacity; i++) {
            System.out.print("enter please word: ");
            result = result && super.add(doubleValue());
            if (!result) break;
        }
        return result;
    }
}
