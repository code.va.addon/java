/* Минимальное из N чисел(использовать LinkedList):
1. Ввести с клавиатуры число N.
2. Считать N целых чисел и заполнить ими список - метод getIntegerList.
3. Найти минимальное число среди элементов списка - метод getMinimum. */
package minumber;

import java.util.LinkedList;
import java.util.Scanner;

class MinNumber extends LinkedList<Integer> {
    private static final Scanner INPUT;
    private int maxSize;

    static {
        INPUT = new Scanner(System.in);
    }

    MinNumber(int maxSize) {
        super();
        this.maxSize = maxSize;
    }
    public static void main(String[] args) {
        System.out.print("enter please max size: ");
        MinNumber number = new MinNumber(INPUT.nextInt());
        number.getIntegerList();
        System.out.println("min = " + number.getMinimum());
    }

    void getIntegerList() {
        for (int i = 1; i <= maxSize; i++) {
            System.out.print("enter please number: ");
            add(INPUT.nextInt());
        }
    }

    int getMinimum() {
        sort(null);
        return get(0);
    }
}
