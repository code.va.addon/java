/* Вводить с клавиатуры строки, пока пользователь не введёт строку 'end':
1. Создать список строк.
2. Ввести строки с клавиатуры и добавить их в список.
3. Вводить с клавиатуры строки, пока пользователь не введёт строку "end". "end" не учитывать.
4. Вывести строки на экран, каждую с новой строки. */
package end;

import java.util.LinkedHashSet;
import java.util.Scanner;

class ExitEnd extends LinkedHashSet<String> {
    private static final Scanner INPUT;

    static {
        INPUT = new Scanner(System.in);
    }
    public static void main(String[] args) {
        ExitEnd setList = new ExitEnd();
        setList.add();
        setList.get();
    }

    void add() {
        while (true) {
            System.out.print("enter please word(\"end\" for exit): ");
            String word = INPUT.next();
            if (word.equals("end")) break;
            super.add(word);
        }
    }

    void get() {
        for (String word : this) {
            System.out.println(word);
        }
    }
}
