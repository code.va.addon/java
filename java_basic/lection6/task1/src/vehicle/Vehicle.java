/* Требуется:
Создать класс Vehicle с методом print. В классе Vehicle, создайте внутренний класс Wheel и Door, которые
также должны содержать метод print. Создайте экземпляры классов Wheel и Door. */
package vehicle;

class Vehicle {

    private class Wheel {
        private void print() {
            System.out.println("class Wheel");
        }
    }

    private class Door {
        private void print() {
            System.out.println("class Door");
        }
    }
    public static void main(String[] args) {
        Vehicle.Wheel wheel = new Vehicle().new Wheel();
        Vehicle.Door door = new Vehicle().new Door();
        Vehicle vehicle = new Vehicle();

        vehicle.print();
        wheel.print();
        door.print();
    }

    private void print() {
        System.out.println("class Vehicle");
    }
}
