/* Создать класс Calculator, с методами для выполнения основных арифметических операций.
Написать программу, которая выводит на экран основные арифметические операции. */
package calculator;

import java.util.Scanner;

public class Calculator {
    private static double number1;
    private static double number2;
    private static Scanner input;

    static {
        input = new Scanner(System.in);

        System.out.print("enter please number1: ");
        number1 = input.nextDouble();
        System.out.print("enter please number2: ");
        number2 = input.nextDouble();
        input.close();
    }

    public static void main(String[] args) {
        plus();
        minus();
        multiplication();
        division();
    }

    private static void plus() {
        System.out.printf("%1$s + %2$s = %3$s\n", number1, number2, number1 + number2);
    }

    private static void minus() {
        System.out.printf("%1$s - %2$s = %3$s\n", number1, number2, number1 - number2);
    }

    private static void multiplication() {
        System.out.printf("%1$s x %2$s = %3$s\n", number1, number2, number1 * number2);
    }

    private static void division() {
        if (number2 != 0) System.out.printf("%1$s : %2$s = %3$s\n", number1, number2, number1 / number2);
        else System.out.println("is not divisible by zero");
    }
}
