package q4;

class A {
    public static int number = 7;

    public A(int n) {
        this.number = n;
    }
}

public class Q4 {
    public static void main(String[] args) {
        A instance = new A(10);
        System.out.println(A.number);
    }
}
