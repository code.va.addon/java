package q3;

class OuterClass {
    public OuterClass() {
        System.out.println("OuterClass constructor");
    }

    public class InnerClass {
        public InnerClass() {
            System.out.println("InnerClass constructor");
        }
    }
}

public class Q3 {
    public static void main(String[] args) {
        OuterClass.InnerClass obj = new OuterClass().new InnerClass();
    }
}
