/* Создать класс Distance с полем distance типа double и методом print. В классе Distance, создайте
статический класс Converter с методами, которые будут конвертировать расстояние в разные единицы
измерения (к примеру, из метров в километры, из километров в мили, и так далее). */
package distance;

import java.util.Scanner;

class Distance {
    private double distance;
    private static Scanner input;

    static {
        input = new Scanner(System.in);
    }

    Distance() {
        System.out.print("enter please distance: ");
        distance = input.nextDouble();
    }

    public static void main(String[] args) {
        Distance dist = new Distance();
        Converter conv = new Converter();
        dist.print(conv);
        Distance dist2 = new Distance();
        dist2.print(conv);
        dist.print(conv);
    }

    private void print(Converter conv) {
        conv.kilometr(distance);
        conv.mili();
    }

    private static class Converter {
        private static double dist;

        private void kilometr(double distance) {
            dist = distance / 1000;
            System.out.printf("%1$sm = %2$skm\n", distance, dist);
        }

        private void mili() {
            System.out.printf("%1$skm = %2$sml\n", dist, dist *= 0.621371);
        }
    }
}
