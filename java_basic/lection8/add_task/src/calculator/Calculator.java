/* Создайте класс Calculator.
В теле класса создайте четыре метода для арифметических действий: (add – сложение, sub – вычитание,
mul – умножение, div – деление).
Метод деления должен делать проверку деления на ноль, если проверка не проходит, сгенерировать
исключение.
Пользователь вводит значения, над которыми хочет произвести операцию и выбрать саму операцию. При
возникновении ошибок должны выбрасываться исключения. */
package calculator;

import java.util.Scanner;

class ErrorDivZero extends Exception {
    ErrorDivZero() {
        super("Error: div on zero");
    }
}

class ErrorMathOperand extends Exception {
    ErrorMathOperand() {
        super("Error: unknown math operand");
    }
}

class Calculator {
    private static double number1, number2;
    private static String math;
    private static final Scanner INPUT = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            System.out.print("enter please number1: ");
            number1 = INPUT.nextDouble();

            System.out.print("enter please number2: ");
            number2 = INPUT.nextDouble();

            System.out.print("enter please add, sub, mul or div: ");
            math = INPUT.next();

            switch (math) {
                case "add":
                    add();
                    break;
                case "sub":
                    sub();
                    break;
                case "mul":
                    mul();
                    break;
                case "div":
                    div();
                    break;
                default:
                    throw new ErrorMathOperand();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void add() {
        System.out.printf("%1$s + %2$s = %3$s\n", number1, number2, number1 + number2);
    }

    private static void sub() {
        System.out.printf("%1$s - %2$s = %3$s\n", number1, number2, number1 - number2);
    }

    private static void mul() {
        System.out.printf("%1$s x %2$s = %3$s\n", number1, number2, number1 * number2);
    }

    private static void div() throws ErrorDivZero {
        if (number2 == 0) throw new ErrorDivZero();
        System.out.printf("%1$s : %2$s = %3$s\n", number1, number2, number1 / number2);
    }
}
