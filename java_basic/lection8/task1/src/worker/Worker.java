/* Описать класс с именем Worker, содержащую следующие поля:
* фамилия и инициалы работника;
* название занимаемой должности;
* год поступления на работу.
Написать программу, выполняющую следующие действия:
* ввод с клавиатуры данных в массив, состоящий из пяти элементов типа Worker (записи должны
быть упорядочены по алфавиту);
* если значение года введено не в соответствующем формате выдает исключение.
* вывод на экран фамилии работника, стаж работы которого превышает введенное значение. */
package worker;

import java.util.Scanner;

class Worker {
    String fullName, profession;
    int year;
    static final int EXPERIENCE = 30;
    static final Scanner INPUT = new Scanner(System.in);
    static int realYear;

    static {
        realYear = 2023;
    }

    public static void main(String[] args) {
        Worker[] workers = new Worker[5];

        for (int i = 0; i < workers.length; i++) {
            new Worker(workers);
        }

        /* for (Worker worker : workers) {
            System.out.println(worker.fullName);
        } */
        for (Worker worker : workers) {
            if (realYear - worker.year > EXPERIENCE) {
                System.out.println(worker.fullName);
            } 
        }
    }

    Worker(Worker[] workers) {
        System.out.print("enter please full name: ");
        fullName = INPUT.nextLine();

        System.out.print("enter please profession: ");
        profession = INPUT.next();

        while (true) {
            try {
                validYear();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }
            break;
        }
        addObject(workers);
    }

    private void addObject(Worker[] workers) {
        Worker worker = this;
        Worker tempWorker = null;

        for (int i = 0; i < workers.length; i++) {
            if (workers[i] == null) {
                workers[i] = worker;
                break;
            }
            else if (equals(worker.fullName, workers[i].fullName)) {
                tempWorker = workers[i];
                workers[i] = worker;
                worker = tempWorker;
            }
        }
    }

    private boolean equals(String str1, String str2) {
        boolean result = false;
        int length = (str1.length() < str2.length()) ? str1.length() : str2.length();

        for (int i = 0; i < length; i++) {
            if (str1.charAt(i) == str2.charAt(i)) continue;
            if (str1.charAt(i) < str2.charAt(i)) result = true;
            break;
        }
        return result;
    }

    private void validYear() throws Exception {
        System.out.print("enter please year: ");
        year = INPUT.nextInt();
        INPUT.nextLine();
        if (realYear - year > 60 || year > realYear) throw new Exception("not correct year");
    }
}
