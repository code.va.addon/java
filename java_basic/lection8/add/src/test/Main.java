package test;

public class Main {
    public static void main(String... args) {
        System.out.println("" + new Main().summ(3, 3));
    }

    public int summ(int a, int b) {
        try {
            return a + b;
        } finally {
            if (a == b) return 0;
        }
    }
}
