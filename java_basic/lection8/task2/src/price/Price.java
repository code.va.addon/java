/* Описать класс с именем Price, содержащую следующие поля:
* название товара;
* название магазина, в котором продается товар;
* стоимость товара в гривнах.
Написать программу, выполняющую следующие действия:
* ввод с клавиатуры данных в массив, состоящий из двух элементов типа Price (записи должны
быть упорядочены в алфавитном порядке по названиям магазинов);
* вывод на экран информации о товарах, продающихся в магазине, название которого введено с
клавиатуры (если такого магазина нет, вывести исключение). */
package price;

import java.util.Scanner;

class Price {
    private String commodity, shop;
    private double price;
    private static final Scanner INPUT = new Scanner(System.in);

    public static void main(String[] args) {
        Price[] prices = new Price[2];

        for (Price pr : prices) {
            pr = new Price(prices);
        }
        try {
            printShop(prices);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void printShop(Price[] prices) throws Exception {
        System.out.print("enter please name shop: ");
        String shop = INPUT.nextLine();
        boolean error = true;

        for (Price price : prices) {
            if (price.shop.equals(shop)) {
                System.out.printf("%1$s:\n%2$s %3$sUAH\n", shop, price.commodity, price.price);
                error = false;
            }
        }
        if (error) throw new Exception("unknown name shop");
    }

    Price(Price[] prices) {
        System.out.print("enter please name shop: ");
        shop = INPUT.nextLine();

        System.out.print("enter please name commodity: ");
        commodity = INPUT.nextLine();

        System.out.print("enter please price: ");
        price = INPUT.nextDouble();
        INPUT.nextLine();
        addObject(prices);
    }

    private void addObject(Price[] prices) {
        Price pr = this;
        Price tempPr = null;

        for (int i = 0; i < prices.length; i++) {
            if (prices[i] == null) {
                prices[i] = pr;
                break;
            }
            else if (equals(pr.shop, prices[i].shop)) {
                tempPr = prices[i];
                prices[i] = pr;
                pr = tempPr;
            }
        }
    }

    private boolean equals(String str1, String str2) {
        boolean result = false;
        int length = (str1.length() < str2.length()) ? str1.length() : str2.length();

        for (int i = 0; i < length; i++) {
            if (str1.charAt(i) == str2.charAt(i)) continue;
            if (str1.charAt(i) < str2.charAt(i)) result = true;
            break;
        }
        return result;
    }
}
