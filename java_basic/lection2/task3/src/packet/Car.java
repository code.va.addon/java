package java_basic.lection2.task3.src.packet;


class Car {
    private int year, weight;
    private double speed;
    private String color;

    Car() {
        this(2023, 500, 100.5, "red");
    }

    Car(int year) {
        this(year, 500);
    }

    Car(int year, int weight) {
        this(year, weight, 100.5);
    }

    Car(int year, int weight, double speed) {
        this(year, weight, speed, "red");
    }

    Car(int year, int weight, double speed, String color) {
        this.year = year;
        this.weight = weight;
        this.speed = speed;
        this.color = color;
    }

    void show() {
        System.out.println("color: " + this.color);
        System.out.println("weight: " + this.weight);
        System.out.println("speed: " + this.speed);
        System.out.println("year: " + this.year);
    }
}
