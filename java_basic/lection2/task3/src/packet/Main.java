/* Создать класс Машина с полями год(int), скорость(double), вес(int) цвет(String).
Создать конструктор по умолчанию, конструктор с 1-м параметром, 2-я, 3-я, 4-я.
Перегрузить конструкторы вызывая конструктор из конструктора.
Создать класс Main, в котором создать экземляры класса Машина с разными параметрами. */
package java_basic.lection2.task3.src.packet;

import java.util.Scanner;

public class Main {
    private static int prompt(String prom, Scanner input) {
        System.out.print("enter please " + prom + ": ");
        return input.nextInt();
    }
    public static void main(String[] args) {
        int year = 0, weight = 0;
        double speed = 0.0;
        String color = "";
        Scanner input = new Scanner(System.in);

        Car car1 = new Car();
        car1.show();

        year = prompt("year", input);
        Car car2 = new Car(year);
        car2.show();

        weight = prompt("weight", input);
        Car car3 = new Car(year, weight);
        car3.show();

        System.out.print("enter please speed: ");
        speed = input.nextDouble();
        Car car4 = new Car(year, weight, speed);
        car4.show();

        System.out.print("enter please color: ");
        color = input.next();
        Car car5 = new Car(year, weight, speed, color);
        car5.show();
    }
}
