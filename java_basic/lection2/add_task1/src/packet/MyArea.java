package java_basic.lection2.add_task1.src.packet;


class MyArea {
    static private final float PI = 3.14f;

    static void areaOfCircle(float radius) {
        float area = PI * (float)Math.pow(radius, 2);
        System.out.println("area: " + area);
    }
}
