/* Создать класс MyArea, в нем объявить константу PI = 3.14 и статический метод areaOfCircle, который
должен принимать радиус и используя PI посчитать площадь круга.
Создать класс Main, где запустить данный метод. */
package java_basic.lection2.add_task1.src.packet;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("enter please radius: ");
        MyArea.areaOfCircle(input.nextFloat());
    }
}
