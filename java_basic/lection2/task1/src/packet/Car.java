package java_basic.lection2.task1.src.packet;


public class Car {
   int year;
   String color;
   
   Car() {
    this(2023, "red");
   }

   Car(int year) {
    this(year, "red");
   }

   Car(int year, String color) {
    this.year = year;
    this.color = color;
   }
}
