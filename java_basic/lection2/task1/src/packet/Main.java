/* Создать класс Машина с полями год(int), цвет(String).
Создать конструктор по умолчанию, конструктор с одним и 2-я параметрами.
Создать класс Main в котором создать экземпляры вызывая разные конструкторы. */
package java_basic.lection2.task1.src.packet;

import java.util.Scanner;


public class Main {
   public static void main(String[] args) {
    int year = 0;
    String color = "";
    Scanner input = new Scanner(System.in);

    Car car1 = new Car();
    System.out.printf("color: %1$s; year: %2$s;\n", car1.color, car1.year);
    System.out.print("enter please year: ");
    year = input.nextInt();
    Car car2 = new Car(year);
    System.out.printf("color: %1$s; year: %2$s;\n", car2.color, car2.year);
    System.out.print("enter please color: ");
    color = input.next();
    Car car3 = new Car(year, color);
    System.out.printf("color: %1$s; year: %2$s;\n", car3.color, car3.year);
   } 
}
