package java_basic.lection2.task2.src.packet;

public class Car {
    private int year = 2023, weight = 500;
    private double speed = 100.5;
    private String color = "red";

    Car() {
        year = 2023;
        weight = 500;
        speed = 100.5;
        color = "red";
    }

    Car(int year) {
        this();
        this.year = year;
    }

    Car(int year, int weight) {
        this(year);
        this.weight = weight;
    }

    Car(int year, int weight, double speed) {
        this(year, weight);
        this.speed = speed;
    }

    Car(int year, int weight, double speed, String color) {
        this(year, weight, speed);
        this.color = color;
    }

    void show() {
        System.out.println("color: " + this.color);
        System.out.println("weight: " + this.weight);
        System.out.println("speed: " + this.speed);
        System.out.println("year: " + this.year);
    }
}
