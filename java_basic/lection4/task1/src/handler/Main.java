/* Создайте класс AbstractHandler.
В теле класса создать методы void open(), void create(), void change(), void save().
Создать производные классы XMLHandler, TXTHandler, DOCHandler от базового класса AbstractHandler.
Написать программу, которая будет выполнять определение документа и для каждого формата должны
быть методы открытия, создания, редактирования, сохранения определенного формата документа. */
package handler;

import java.util.Scanner;

public class Main {
    static final Scanner INPUT = new Scanner(System.in);
    public static void main(String[] args) {
        String nameDoc = "unknown document";

        nameDoc = prompt("name document"); 
        if (nameDoc.contains(".xml")) {
            AbstractHandler xml = new XMLHandler();
            document(xml);
        }
        else if (nameDoc.contains(".txt")) {
            AbstractHandler txt = new TXTHandler();
            document(txt);
        }
        else if (nameDoc.contains(".doc")) {
            AbstractHandler doc = new DOCHandler();
            document(doc);
        }
        else {
            System.out.println("unknown format");
        }
    }

    private static String prompt(String prom) {
        System.out.printf("enter please %1$s: ", prom);
        return INPUT.next();
    }

    private static boolean validCommand (String command) {
        boolean result = prompt(command).equals(command);

        if(!result) System.out.println("unknown command");
        return result;
    }

    private static void document(AbstractHandler doc) {
        doc.create();
        if (validCommand("open")) doc.open();
        else return;
        if (validCommand("change")) doc.change();
        else return;
        if (validCommand("save")) doc.save();
    }
}
