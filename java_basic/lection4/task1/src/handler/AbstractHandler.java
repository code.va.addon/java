package handler;

abstract class AbstractHandler {
    abstract void open();
    abstract void create();
    abstract void change();
    abstract void save();
}


class XMLHandler extends AbstractHandler {
    void open() {
        System.out.println("document *.xml is open");
    }

    void create() {
        System.out.println("document *.xml is create");
    }

    void change() {
        System.out.println("document *.xml is change");
    }

    void save() {
        System.out.println("document *.xml is change");
    }
}


class TXTHandler extends AbstractHandler {
    void open() {
        System.out.println("document *.txt is open");
    }

    void create() {
        System.out.println("document *.txt is create");
    }

    void change() {
        System.out.println("document *.txt is change");
    }

    void save() {
        System.out.println("document *.txt is change");
    }
}


class DOCHandler extends AbstractHandler {
    void open() {
        System.out.println("document *.doc is open");
    }

    void create() {
        System.out.println("document *.doc is create");
    }

    void change() {
        System.out.println("document *.doc is change");
    }

    void save() {
        System.out.println("document *.doc is change");
    }
}
