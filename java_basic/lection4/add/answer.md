Даны объявления интерфейсов. Которые из них скомпилируются с ошибкой?
```java
interface Quest21{
    static class Inner{ int x; }
}

class Quest23{
    static interface Inner{
        int x;
    }
}

class Quest22{
    interface Inner{ int x; }
}

interface Quest20{
    class Inner{
        private int x;
    }
}
```

***

Какой результат компиляции и выполнения программы:
```java
interface Interface {
    void print();
}

class Class implements Interface {

    public void print() {
        System.out.println("The Class");
    }
}

public class Main {

    public static void main(String[] args) {
        Class c = new Class();
        Interface i = c;
        i.print();
    }
}
```
[-] Ошибка времени выполнения ClassCastExeption
[-] Ошибка компиляции
[-] В консоль выведет "The Class"
[-] Ошибка времени выполнения NullPointerExeption

***

Вставьте подходящий код для успешной компиляции:
```java
interface Animal {
    void eat();
}
class Feline implements Animal{}

 /* enter your code */

public class HouseCat implements Feline {
    public void eat() {
    }
}
```
[-] class Feline implements Animal{}
[-] interface Feline extends Animal { void eat() { } }
[-] interface Feline extends Animal { }
[-] interface Feline extends Animal { void eat(); }
