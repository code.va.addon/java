package inter;

public interface ITest extends I1, I2 {
    public static final int PRICE = 10;
    public abstract void move();
//java8
    static int sum() {
        return 10 + 8;
    }
    default int mul() {
        return 10 * 5;
    }
}

interface I1 {//functional interface
    void eat();
}
interface I2 {}//interface marker
