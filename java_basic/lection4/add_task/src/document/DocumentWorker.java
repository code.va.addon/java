package java_basic.lection4.add_task.src.document;

import java.util.Scanner;

abstract class Document {
    void openDocument() {
        System.out.println("Document open");
    }
    abstract void editDocument();
    abstract void saveDocument();
}

class DocumentWorker extends Document {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Document doc = null;

        System.out.print("enter please key: ");
        switch (input.next()) {
            case "key_pro":
                doc = new ProDocumentWorker();
                break;
            case "key_exp":
                doc = new ExpertDocumentWorker();
                break;
            default:
                doc = new DocumentWorker();
                break;
        }
        doc.openDocument();
        doc.editDocument();
        doc.saveDocument();
    }

    @Override
    void editDocument() {
        System.out.println("Edit document open in version Pro");
    }

    @Override
    void saveDocument() {
        System.out.println("Save document open in version Pro");
    }
}

class ProDocumentWorker extends Document {
    @Override
    void editDocument() {
        System.out.println("Document edit");
    }

    @Override
    void saveDocument() {
        System.out.println(
            "Document save in old format.\n" +
            "Save in new formats open in version Expert."
        );
    }
}

class ExpertDocumentWorker extends Document {
    @Override
    void editDocument() {
        System.out.println("Document edit");
    }

    @Override
    void saveDocument() {
        System.out.println("Documnet save in new format");
    }
}
