/* Создайте 2 интерфейса Playable и Recodable. В каждом из интерфейсов создайте по 3 метода void play() /
void pause() / void stop() и void record() / void pause() / void stop() соответственно.
Создайте производный класс Player от базовых интерфейсов Playable и Recodable.
Написать программу, которая выполняет проигрывание и запись. */
package record;

import java.util.Scanner;

interface Playable {
    void play();
    void pause();
    void stop();
}

interface Recodable {
    void record();
    void pause();
    void stop();
}

class Player implements Playable, Recodable {
    public static void main(String[] args) {
        final Scanner INPUT = new Scanner(System.in);

        System.out.print("enter please player or record: ");
        switch (INPUT.next()) {
            case "player":
                Player player = new Player();
                playGo(player, INPUT, "play");
                break;
            case "record":
                Player recorder = new Player();
                playGo(recorder, INPUT, "record");
                break;
            default:
                System.out.println("unknown command");
                break;
        }
    }

    private static void playGo(Player player, Scanner INPUT, String ind) {
        String command = "start";
        String input = " ";

        while (true) {
            if (command.equals("start") && ind.equals("play")) {
                player.play();
                command = "play";
            }
            else if (command.equals("start") && ind.equals("record")) {
                player.record();
                command = "play";
            }
            else if (input.equals("pause") && command.equals("play")) {
                player.pause();
                command = "pause";
            }
            else if (input.equals("play") && command.equals("pause") && ind.equals("play")) {
                player.play();
                command = "play";
            }
            else if (input.equals("play") && command.equals("pause") && ind.equals("record")) {
                player.record();
                command = "play";
            }
            else if (input.equals("stop") && (command.equals("play") || command.equals("pause"))) {
                player.stop();
                break;
            }
            else {
                System.out.println("unknown command");
                break;
            }
            input = INPUT.next();
        }
    }

    public void play() {
        System.out.println("play");
        System.out.print("enter please stop or pause: ");
    }

    public void record() {
        System.out.println("record");
        System.out.print("enter please stop or pause: ");
    }

    public void pause() {
        System.out.println("pause");
        System.out.print("enter please stop or play: ");
    }

    public void stop() {
        System.out.println("stop");
    }
}
