package java_basic.lection3.task1.src.pack;

class Pupil {
    private String name;

    Pupil() {}

    Pupil(String name) {
        this.name = name;
    }

    void getName() {
        System.out.println(name + ":");
    }

    void study() {
        System.out.println("study method base class");
    }

    void read() {
        System.out.println("read method base class");
    }

    void write() {
        System.out.println("write method base class");
    }

    void relax() {
        System.out.println("relax method base class");
    }
}
