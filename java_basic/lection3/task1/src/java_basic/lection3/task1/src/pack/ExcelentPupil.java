package java_basic.lection3.task1.src.pack;

class ExcelentPupil extends Pupil {
    ExcelentPupil(String name) {
        super(name);
    }

    @Override
    void study() {
        System.out.println("excelent study");
    }

    @Override
    void read() {
        System.out.println("excelent read");
    }

    @Override
    void write() {
        System.out.println("excelent write");
    }

    @Override
    void relax() {
        System.out.println("bad relax");
    }
}
