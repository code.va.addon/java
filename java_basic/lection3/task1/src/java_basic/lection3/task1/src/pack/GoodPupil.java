package java_basic.lection3.task1.src.pack;

class GoodPupil extends Pupil {
    GoodPupil(String name) {
        super(name);
    }

    @Override
    void study() {
        System.out.println("good study");
    }

    @Override
    void read() {
        System.out.println("good read");
    }

    @Override
    void write() {
        System.out.println("good write");
    }

    @Override
    void relax() {
        System.out.println("good relax");
    }
    
}
