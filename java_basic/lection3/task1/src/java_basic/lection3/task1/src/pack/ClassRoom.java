/* Создать класс, представляющий учебный класс ClassRoom.
Создайте класс ученик Pupil. В теле класса создайте методы void study(), void read(), void write(), void relax().
Создайте 3 производных класса ExcelentPupil, GoodPupil, BadPupil от класса базового класса Pupil и
переопределите каждый из методов, в зависимости от успеваемости ученика.
Конструктор класса ClassRoom принимает аргументы типа Pupil, класс должен состоять из 4 учеников.
Предусмотрите возможность того, что пользователь может передать 2 или 3 аргумента.
Выведите информацию о том, как все ученики экземпляра класса ClassRoom умеют учиться, читать, писать,
отдыхать. */
package java_basic.lection3.task1.src.pack;

class ClassRoom {
    ClassRoom(Pupil[] excelent, Pupil[] good) {
        ClassRoom.getPupils(excelent);
        ClassRoom.getPupils(good);
    }

    ClassRoom(Pupil[] excelent, Pupil[] good, Pupil[] bad) {
        ClassRoom.getPupils(excelent);
        ClassRoom.getPupils(good);
        ClassRoom.getPupils(bad);
    }

    private static void getPupils(Pupil[] pupils) {
        for (Pupil pupil : pupils) {
            pupil.getName();
            pupil.study();
            pupil.read();
            pupil.write();
            pupil.relax();
        }
    }

    public static void main(String[] args) {
        /* Pupil[] excelents = {new ExcelentPupil("John"), new ExcelentPupil("Evelina")};
        Pupil[] goods = {new GoodPupil("Mike"), new GoodPupil("Edvard")};
        new ClassRoom(excelents, goods); */

        Pupil[] excelent = {new ExcelentPupil("Evelina")};
        Pupil[] good = {new GoodPupil("Mike"), new GoodPupil("Edvard")};
        Pupil[] bad = {new BadPupil("John")};
        new ClassRoom(excelent, good, bad);
    }
}
