package java_basic.lection3.task1.src.pack;

class BadPupil extends Pupil {
    BadPupil(String name) {
        super(name);
    }
    
    @Override
    void study() {
        System.out.println("bad study");
    }

    @Override
    void read() {
        System.out.println("bad read");
    }

    @Override
    void write() {
        System.out.println("bad write");
    }

    @Override
    void relax() {
        System.out.println("excelent relax");
    }
}
