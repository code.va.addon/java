/* Требуется:
Создать класс Vehicle.
В теле класса создайте поля: координаты и параметры средств передвижения (цена, скорость, год
выпуска).
Создайте 3 производных класса Plane, Саг и Ship.
Для класса Plane должна быть определена высота и количество пассажиров.
Для класса Ship – количество пассажиров и порт приписки.
Написать программу, которая выводит на экран информацию о каждом средстве передвижения. */
package java_basic.lection3.task2.src.vehicle;

import java.util.Scanner;

class Vehicle {
    private float coordinates, price;
    private int speed, year;
    Scanner input;

    public static void main(String[] args) {
        Car car = new Car();
        Plane plane = new Plane();
        Ship ship = new Ship();

        car.show();
        plane.show();
        ship.show();
    }

    Vehicle(String vehicle) {
        this.input = new Scanner(System.in);

        System.out.println(vehicle);

        System.out.print("enter please coordinates: ");
        this.coordinates = this.input.nextFloat();

        System.out.print("enter please price: ");
        this.price = this.input.nextFloat();

        System.out.print("enter please speed: ");
        this.speed = this.input.nextInt();

        System.out.print("enter please year: ");
        this.year = this.input.nextInt();
    }

    void show() {
        System.out.println("coordinates: " + this.coordinates);
        System.out.println("price: " + this.price);
        System.out.println("speed: " + this.speed);
        System.out.println("year: " + this.year);
    }
}
