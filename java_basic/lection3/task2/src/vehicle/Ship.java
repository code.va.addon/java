package java_basic.lection3.task2.src.vehicle;

class Ship extends Vehicle {
    private int passengers;
    private String port;

    Ship() {
        super("Ship");
        System.out.print("enter please passengers: ");
        this.passengers = this.input.nextInt();

        System.out.print("enter please port: ");
        this.port = this.input.next() + this.input.nextLine();
    }

    @Override
    void show() {
        System.out.println("Ship");
        super.show();
        System.out.println("passengers: " + this.passengers);
        System.out.println("port: " + this.port);
    }
}
