package java_basic.lection3.task2.src.vehicle;

class Car extends Vehicle {

    Car() {
        super("Car");
    }

    @Override
    void show() {
        System.out.println("Car");
        super.show();
    }
}
