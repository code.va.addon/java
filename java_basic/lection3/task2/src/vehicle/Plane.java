package java_basic.lection3.task2.src.vehicle;

class Plane extends Vehicle {
    private int height, passengers;

    Plane() {
        super("Plane");
        System.out.print("enter please height: ");
        this.height = this.input.nextInt();

        System.out.print("enter please passengers: ");
        this.passengers = this.input.nextInt();
    }

    @Override
    void show() {
        System.out.println("Plane");
        super.show();
        System.out.println("heigh: " + this.height);
        System.out.println("passengers: " + this.passengers);
    }
}
