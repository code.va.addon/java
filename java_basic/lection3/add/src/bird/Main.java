package bird;

public class Main {
    public static void main(String[] args) {
        /* Bird s = new Strauss();
        Bird p = new Penguin();
        Bird[] arr = {s, p}; */
        Bird[] arr = {new Strauss(), new Penguin()};

        for (Bird bird : arr) {
            bird.move();
        }
    }
}
