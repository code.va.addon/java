package bird;

public class Penguin extends Bird {
    /* void move() {} //error parent class - public void move(); public > default*/
    
    /* @Override
    public void move(int a) {} //error override */

    @Override
    public void move() {
        System.out.println("Swim");
    }
}
