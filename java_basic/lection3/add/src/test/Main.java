package test;

public class Main {
    Main() {
        System.out.println("Main");
    }//1. run Main()
    
    public static void main(String[] args) {
       Cc c = new Cc(); 
    }
}

class Aa extends Main {
    Aa() {
        System.out.println("Aa");
    }//2. run Aa()
}

class Bb extends Aa {
    Bb() {
        System.out.println("Bb");
    }//3. run Bb()
}

class Cc extends Bb {
    Cc() {
        System.out.println("Cc");
    }//4. run Cc()
}
