package test;

class A {
    {
        System.out.println("In block1");
    }//3.run dynamic block 1

    static {
        System.out.println("In static block1");
    } //1.run static block; initial 1 stap(class)

    A() {//2.read name constructor
        System.out.println("In constructor A");
    }//5.run constructor

    {
        System.out.println("In block2");
    }//4.run dinamyc block 2

    public static void main(String[] args) {
        A a = new A();
        A a2 = new A();
    }
}
