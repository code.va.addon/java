package java_basic.lection3.task3.src.document;

class ProDocumentWorker extends DocumentWorker {
   @Override
   void editDocument() {
    System.out.println("Document edit");
   }

   @Override
   void saveDocument() {
    System.out.println(
        "Document save in old format.\n" +
        "Save in new formats open in version Expert."
    );
   }
}
