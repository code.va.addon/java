/* Требуется:
Создайте класс DocumentWorker.
В теле класса создайте три метода openDocument(), editDocument(), saveDocument().
В тело каждого из методов добавьте вывод на экран соответствующих строк: "Документ открыт",
"Редактирование документа доступно в версии Про", "Сохранение документа доступно в версии Про".
Создайте производный класс ProDocumentWorker.
Переопределите соответствующие методы, при переопределении методов выводите следующие строки:
"Документ отредактирован", "Документ сохранен в старом формате, сохранение в остальных форматах
доступно в версии Эксперт".
Создайте производный класс ExpertDocumentWorker от базового класса ProDocumentWorker.
Переопределите соответствующий метод. При вызове данного метода необходимо выводить на экран
"Документ сохранен в новом формате".
В теле метода main() реализуйте возможность приема от пользователя номера ключа доступа pro и exp.
Если пользователь не вводит ключ, он может пользоваться только бесплатной версией (создается
экземпляр базового класса), если пользователь ввел номера ключа доступа pro и exp, то должен создаться
экземпляр соответствующей версии класса, приведенный к базовому – DocumentWorker. */
package java_basic.lection3.task3.src.document;

import java.util.Scanner;

class DocumentWorker {
    void openDocument() {
        System.out.println("Document open");
    }

    void editDocument() {
        System.out.println("Edit document open in version Pro");
    }

    void saveDocument() {
        System.out.println("Save document open in version Pro");
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        DocumentWorker doc = null;
        String key = "unknown key";

        System.out.print("enter please key: ");
        key = input.nextLine();
        switch (key) {
            case "key_pro":
                doc = new ProDocumentWorker();
                break;
            case "key_exp":
                doc = new ExpertDocumentWorker();
                break;
            default:
                doc = new DocumentWorker();
                break;
        }
        doc.openDocument();
        doc.editDocument();
        doc.saveDocument();
    }
}
