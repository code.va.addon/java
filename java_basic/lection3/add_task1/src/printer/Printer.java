/* Требуется:
Создайте класс Printer.
В теле класса создайте метод void print(String value), который выводит на экран значение аргумента.
Реализуйте возможность того, чтобы в случае наследования от данного класса других классов, и вызове
соответствующего метода их экземпляра, строки, переданные в качестве аргументов методов,
выводились разными цветами.
Обязательно используйте приведение типов. */
package java_basic.lection3.add_task1.src.printer;

class Printer {
    Printer() {}

    public static void main(String[] args) {
        BluePrinter blue = new BluePrinter();
        blue.print("Hello BluePrinter");
        Printer printer = blue;
        printer.print("Hello Printer");
        BluePrinter newBlue = (BluePrinter)printer;
        newBlue.print("Hello BluePrinter");
    }

    void print(String value) {
        System.out.println(value);
    }
}
