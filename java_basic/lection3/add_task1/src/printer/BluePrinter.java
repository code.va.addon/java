package java_basic.lection3.add_task1.src.printer;

class BluePrinter extends Printer {
    BluePrinter() {}

    @Override
    void print(String value) {
        System.out.println("blue");
        super.print(value);
    }
}
