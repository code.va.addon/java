package wisualgen;

public class Main {
    public static void main(String[] args) {
        WithGen w1 = new WithGen("Test");
        WithGen w2 = new WithGen(10);
        WithGen w3 = new WithGen(new Cat("Bars"));

        w1.display(); // Test
        w2.display(); // 10
        w3.display(); // Bars

        w1 = w2;

        String s = (String) w1.getObj();
        System.out.println(s);
    }
}

class WithGen {
    Object obj;

    WithGen(Object obj) {
        this.obj = obj;
    }

    void display() {
        System.out.println(obj);
    }

    Object getObj() {
        return obj;
    }
}

class Cat {
    String name;

    Cat(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
