package variant;

import java.util.AbstractList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Number[] num = new Integer[10];
        num[0] = 10.1d;
        System.out.println(num[0]); //kovariantnyj

        List<Number> list = new AbstractList<Integer>(); //invariantnyj
    }
}
