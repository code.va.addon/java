package bird;

public class Main {
    public static void main(String[] args) {
        Strause[] s = {new Strause(55), new Strause(60)};
        Pinguine[] p = {new Pinguine(15), new Pinguine(18), new Pinguine(20)};
        Logic<Strause> l1 = new Logic<>(s);
        l1.display();
        Logic<Pinguine> l2 = new Logic<>(p);
        l2.display();
        Logic.weightCompare(l1, l2);
    }
}

class Bird {
    int weight;

    Bird(int weight) {
        this.weight = weight;
    }

    void move() {}
}

class Strause extends Bird {
    Strause(int weight) {
        super(weight);
    }

    @Override
    void move() {
        System.out.println("run");
    }
}

class Pinguine extends Bird {
    Pinguine(int weight) {
        super(weight);
    }

    @Override
    void move() {
        System.out.println("swim");
    }
}

class Logic<T extends Bird> {
    T[] array;
    
    Logic(T[] array) {
        this.array = array;
    }

    void display() {
        for (T a : array) {
            a.move();
            System.out.println(a.weight);
        }
    }

    int weightCtrl() {
        int sum = 0;
        for (T a : array) {
            sum += a.weight;
        }
        return sum;
    }

    static void weightCompare(Logic<?> c1, Logic<?> c2) {
        System.out.println(c1.weightCtrl() - c2.weightCtrl());
    }
}
