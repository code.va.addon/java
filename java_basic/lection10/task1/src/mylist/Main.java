/* Создайте класс MyList<T>. Реализуйте в простейшем приближении возможность использования его
экземпляра аналогично экземпляру класса List<T>. Минимально требуемый интерфейс взаимодействия с
экземпляром, должен включать метод добавления элемента, индексатор для получения значения
элемента по указанному индексу и свойство только для чтения для получения общего количества
элементов. */
package mylist;

class Main {
    public static void main(String[] args) {
        MyListInterface<String> listString = new MyList<>();
        listString.add("Hello");
        listString.add("World");
        print(listString);
        MyListInterface<Integer> listInteger = new MyList<>();
        listInteger.add(10);
        listInteger.add(30);
        listInteger.add(45);
        print(listInteger);
    }
    
    private static <T> void print(MyListInterface<T> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println("size: " + list.size());
    }
}

interface MyListInterface <T> {
    void add(T element);
    void add(int index, T element);
    T get(int index);
    int size();
}

class MyList<T> implements MyListInterface<T> {
    private T[] myArray;

    MyList() {
        myArray = (T[]) new Object[1];
    }

    @Override
    public void add(T element) {
        T[] tempArray = (T[]) new Object[myArray.length + 1];

        myArray[myArray.length - 1] = element;
        System.arraycopy(myArray, 0, tempArray, 0, myArray.length);
        myArray = tempArray;
    }

    @Override
    public void add(int index, T element) {
        if (index == myArray.length - 1) add(element);
        else if (index < myArray.length) myArray[index] = element;
        else {
            T[] tempArray = (T[]) new Object[index + 1];
            System.arraycopy(myArray, 0, tempArray, 0, myArray.length);
            tempArray[index] = element;
            myArray = tempArray;
        } 
    }

    @Override
    public T get(int index) {
        T value;
        try {
            value = myArray[index];
        } catch (Exception e) {
            System.out.println("Error: not correct index " + index);
            value = null;
        }
        return value;
    }

    @Override
    public int size() {
        return myArray.length - 1;
    }
}
