/* Создайте класс MyDictionary<TKey,TValue>. Реализуйте в простейшем приближении возможность
использования его экземпляра. Минимально требуемый интерфейс взаимодействия с экземпляром,
должен включать метод добавления пар элементов, индексатор для получения значения элемента по
указанному индексу и свойство только для чтения для получения общего количества пар элементов. */
package mydict;

interface MyDictionaryInterface<TKEY, TVALUE> {
    void add(TKEY key, TVALUE value);
    TVALUE get(TKEY key);
    int size();
    TKEY getKeys(int index);
    TVALUE getValues(int index);
}

class MyDictionary<TKEY, TVALUE> implements MyDictionaryInterface<TKEY, TVALUE> {
    private TKEY[] keys;
    private TVALUE[] values;
    public static void main(String[] args) {
        MyDictionaryInterface<String, String> dict1 = new MyDictionary<>();
        dict1.add("key1", "value1");
        dict1.add("key2", "value2");
        print(dict1);

        MyDictionaryInterface<String, Integer> dict2 = new MyDictionary<>();
        dict2.add("key1", 1);
        dict2.add("key2", 2);
        dict2.add("key3", 3);
        print(dict2);

        MyDictionaryInterface<Integer, String> dict3 = new MyDictionary<>();
        dict3.add(0, "value1");
        dict3.add(1, "value2");
        dict3.add(2, "value3");
        dict3.add(3, "value4");
        print(dict3);
    }

    private static <TKEY, TVALUE> void print(MyDictionaryInterface<TKEY, TVALUE> dict) {
        for (int i = 0; i < dict.size(); i++) {
            System.out.println(dict.getKeys(i) + " : " + dict.getValues(i));
        }
        System.out.println("size: " + dict.size());
    }

    MyDictionary() {
        keys = (TKEY[]) new Object[1];
        values = (TVALUE[]) new Object[1];
    }

    @Override
    public void add(TKEY key, TVALUE value) {
        TKEY[] tempKeyArray = (TKEY[]) new Object[keys.length + 1];
        TVALUE[] tempValueArray = (TVALUE[]) new Object[values.length + 1];

        keys[keys.length - 1] = key;
        values[values.length - 1] = value;
        System.arraycopy(keys, 0, tempKeyArray, 0, keys.length);
        System.arraycopy(values, 0, tempValueArray, 0, values.length);
        keys = tempKeyArray;
        values = tempValueArray;
    }

    @Override
    public TVALUE get(TKEY key) {
        TVALUE value = null;

        for(int i = 0; i < keys.length; i++) {
            if (keys[i].equals(key)) value = values[i];
        }
        return value;
    }

    @Override
    public int size() {
        return values.length - 1;
    }

    @Override
    public TKEY getKeys(int index) {
        return keys[index];
    }

    @Override
    public TVALUE getValues(int index) {
        return values[index];
    }
}
