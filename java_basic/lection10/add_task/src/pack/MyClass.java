/* Создайте класс MyClass<T>, содержащий статический фабричный метод - T factoryMethod(), который
будет порождать экземпляры типа, указанного в качестве параметра типа (указателя места заполнения
типом – Т). */
package pack;

class MyClass {
    public static void main(String[] args) {
        factoryMethod(new Integer(10));
    }

    private static <T> void factoryMethod(T obj) {
        System.out.println(obj.toString());
    }
}
