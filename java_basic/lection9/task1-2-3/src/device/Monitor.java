package device;

class Monitor extends Device {
    private int resolutionX, resolutionY, result;

    Monitor(int resolutionX, int resolutionY, String manufacturer, String serialNumber, float price) {
        super(manufacturer, serialNumber, price);
        this.resolutionX = resolutionX;
        this.resolutionY = resolutionY;
        result = super.hashCode();
    }

    void setResolutionX(int resolutionX) {
        this.resolutionX = resolutionX;
    }

    int getResolutionX() {
        return resolutionX;
    }

    void setResolutionY(int resolutionY) {
        this.resolutionY = resolutionY;
    }

    int getResolutionY() {
        return resolutionY;
    }

    @Override
    public String toString() {
        String parentString = super.toString();
        return parentString + String.format(", X = %1$d, Y = %2$d", resolutionX, resolutionY);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) return false;
        Monitor ea = (Monitor) obj;
        if (resolutionX != ea.resolutionX || resolutionY != ea.resolutionY || !super.equals(obj)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = 37 * this.result + resolutionX;
        result = 37 * result + resolutionY;
        return result;
    }
}
