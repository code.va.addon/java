package device;

class Device {
    private String manufacturer, serialNumber;
    private float price;
    private int result;

    Device(String manufacturer, String serialNumber, float price) {
        this.manufacturer = manufacturer;
        this.serialNumber = serialNumber;
        this.price = price;
        result = super.hashCode();
    }

    void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    String getManufacturer() {
        return manufacturer;
    }

    void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    String getSerialNumber() {
        return serialNumber;
    }

    void setPrice(float price) {
        this.price = price;
    }

    float getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return String.format("manufacturer = %s, price = %f, serialNumber=%s", manufacturer, price, serialNumber);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) return false;
        Device ea = (Device) obj;
        if (!manufacturer.equals(ea.manufacturer) || !serialNumber.equals(ea.serialNumber) || price != ea.price) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = 37 * this.result + Float.floatToIntBits(price);
        result = 37 * result + (manufacturer == null ? 0 : manufacturer.hashCode());
        result = 37 * result + (serialNumber == null ? 0 : serialNumber.hashCode());
        return result;
    }
}
