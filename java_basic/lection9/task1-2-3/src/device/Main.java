/* Создать классы:
1) Основной класс Device (manufacturer(String), price(float), serialNumber(String));
2) Сабкалсс Monitor (resolutionX(int), resolutionY(int)) and EthernetAdapter (speed (int), mac (String));
Добавить методы доступа. Конструктор.
=============================================================================
В обоих классах переопределить метод toString, что бы вывод был следующим:
Device: manufacturer =Samsung, price=120, serialNumber=AB1234567CD
Monitor: manufacturer =Samsung, price=120, serialNumber=AB1234567CD, X=1280,Y=1024
=============================================================================
Переопределить методы equals & hashCode в каждом классе.
Создать класс Main, в котором создать объекты классов и продемонстрировать переопределенные
методы. */
package device;

class Main {
    public static void main(String[] args) {
        Device mnt1 = new Monitor(100, 500, "Samsung", "AB1234567CD", 120);
        Device mnt2 = new Monitor(100, 500, "Samsung", "AB1234567CD", 120);
        Device mnt3 = new Monitor(150, 800, "Samsung", "AB1234567CD", 120);
        Device ea1 = new EthernetAdapter(120, "k2j3k23jk2j3", "Samsung", "AB1234567CD", 120);

        System.out.println("Monitor: " + mnt1);
        System.out.println("Enthernet adapter: " + ea1 + "\n");

        System.out.println(mnt1.equals(mnt2));
        System.out.println(mnt1.equals(mnt1));
        System.out.println(mnt2.equals(mnt3));
        System.out.println(mnt1.equals(ea1));
        System.out.println(mnt2.equals(new Object()) + "\n");

        System.out.println(mnt1.hashCode() == mnt2.hashCode());
        System.out.println(mnt1.hashCode() == mnt1.hashCode());
        System.out.println(mnt1.hashCode() == mnt3.hashCode());
        System.out.println(mnt2.hashCode() == ea1.hashCode());
    }
}
