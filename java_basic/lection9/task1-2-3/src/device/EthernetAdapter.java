package device;

class EthernetAdapter extends Device {
    private int speed, result;
    private String mac;

    EthernetAdapter(int speed, String mac,String manufacturer, String serialNumber, float price) {
        super(manufacturer, serialNumber, price);
        this.speed = speed;
        this.mac = mac;
        result = super.hashCode();
    }

    void setSpeed(int speed) {
        this.speed = speed;
    }

    int getSpeed() {
        return speed;
    }

    void setMac(String mac) {
        this.mac = mac;
    }

    String getMac() {
        return mac;
    }

    @Override
    public String toString() {
        String parentString = super.toString();
        return parentString + String.format(", speed = %1$d, mac = %s", speed, mac);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) return false;
        EthernetAdapter ea = (EthernetAdapter) obj;
        if (speed != ea.speed || !mac.equals(ea.mac) || !super.equals(obj)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = 37 * this.result + speed;
        result = 37 * result + (mac == null ? 0 : mac.hashCode());
        return result;
    }
}
