/* Создать класс Animal с именем String, возрастом int, хвостом Boolean. В классе переопределить метод
toString, что бы вывод был сдедующим
«Имя: Васька, возраст: 45, хвост: нет».
В классе Animal переопределить методы equals & hashCode. */
package animal;

class Animal {
   private String name;
   private int year, result;
   private boolean tail;
   private static int ind;

   public static void main(String[] args) {
    Animal cat1 = new Animal("Boris", 3, true);
    Animal cat2 = new Animal("Boris", 3, true);
    Animal cat3 = new Animal("Barsik", 2, true);
    Animal dog1 = new Animal("Boris", 3, true);

    System.out.println(cat1.equals(cat1));
    System.out.println(cat1.equals(cat2));
    System.out.println(cat1.equals(cat3));
    System.out.println(cat1.equals(dog1) + "\n");

    System.out.println(cat1.hashCode() == cat1.hashCode());
    System.out.println(cat1.hashCode() == cat2.hashCode());
    System.out.println(cat1.hashCode() == cat3.hashCode());
    System.out.println(cat1.hashCode() == dog1.hashCode());
   }
   
   Animal(String name, int year, boolean tail) {
    this.name = name;
    this.year = year;
    this.tail = tail;
    this.result = ++Animal.ind;
   }

   @Override
   public String toString() {
    String tail = (this.tail) ? "yes" : "no";
    return String.format("Name: %1$s, age: %2$d, tail: %3$s", this.name, this.year, tail);
   }

   @Override
   public boolean equals(Object obj) {
    if (obj == null || getClass() != obj.getClass()) return false;
    Animal anm = (Animal) obj;
    if (name != anm.name || year != anm.year || tail != anm.tail) return false;
    return true;
   }

   @Override
   public int hashCode() {
    int result = 37 * this.result + year;
    result = 37 * result + (name == null ? 0 : name.hashCode());
    result = 37 * result + (tail ? 1 : 0);
    return result;
   }
}
