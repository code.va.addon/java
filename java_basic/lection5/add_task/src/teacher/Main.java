/* Создать class Main, в нем создать список, добавить учителей, которых вы только сможете вспомнить со
школы. И получить индекс самого лучшего учителя и самого не очень. Вывести список в консоль. */
package teacher;

import java.util.ArrayList;

class Main {
    public static void main(String[] args) {
        ArrayList <String> teachers = new ArrayList<>();

        teachers.add("Ludmila");
        teachers.add("Ganna");
        teachers.add("Inna");
        teachers.add("Svitlana");

        System.out.println("index bad teacher: " + teachers.indexOf("Ludmila"));
        System.out.println("index good teacher: " + teachers.indexOf("Svitlana"));
        System.out.println("teachers: " + teachers);
    }
}
