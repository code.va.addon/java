/* Создать класс Main, создать список целых чисел и используя ListIterator пройтись по списку и увеличить
значения на 1; */
package listpack;

import java.util.ArrayList;
import java.util.ListIterator;

class Main {
    public static void main(String[] args) {
        ArrayList <Integer> intList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            intList.add(i, i + 1);
        }
        System.out.println("list: " + intList);

        ListIterator <Integer> listIterator = intList.listIterator();
        while (listIterator.hasNext()) {
            Integer val = listIterator.next();
            listIterator.set(++val);
        }
        System.out.println("new list: " + intList);
    }
}
