/* Создать класс Zoo. В классе создать список, в который записать 8 животных через метод add(index,
element).
Вывести список в консоль.
Используя класс Zoo Задания 2, удалить 3, 5, 7 животные, узнать размер списка и вывести в консоль.*/
package zoo;

import java.util.ArrayList;

class Zoo {
    public static void main(String[] args) {
        ArrayList <String> zoo = new ArrayList<>(8);

        zoo.add(0, "wolf");
        zoo.add(1, "bear");
        zoo.add(2, "turple");
        zoo.add(3, "elefan");
        zoo.add(4, "hare");
        zoo.add(5, "fox");
        zoo.add(6, "giraffe");
        zoo.add(7, "tiger");

        System.out.println(zoo);

        zoo.remove(3);
        zoo.remove(4);
        zoo.remove(5);

        System.out.println("length zoo: " + zoo.size());
    }
}
