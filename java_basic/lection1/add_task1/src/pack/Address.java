/* Требуется: Создать класс с именем Address. В теле класса требуется создать поля: index, country, city,
street, house, apartment.
Для каждого поля, создать метод с двумя методами доступа (get, set)
Создать экземпляр класса Address.
В поля экземпляра записать информацию о почтовом адресе.
Выведите на экран значения полей, описывающих адрес. */
package java_basic.lection1.add_task1.src.pack;

import java.util.Scanner;


public class Address {
    private int index, house, apartment;
    private String country, city, street;
    private Scanner input;

    Address() {
        this.input = new Scanner(System.in);
    }
    public static void main(String[] args) {
        Address address = new Address();

        address.setCountry();
        address.setCity();
        address.setIndex();
        address.setStreet();
        address.setHouse();
        address.setApartment();

        address.getCountry();
        address.getCity();
        address.getIndex();
        address.getStreet();
        address.getHouse();
        address.getApartment();
    }

    private void setIndex() {
        System.out.print("enter please index: ");
        this.index = this.input.nextInt();
    }

    private void getIndex() {
        System.out.println("index: " + this.index);
    }

    private void setCity() {
        System.out.print("enter please city: ");
        this.city = this.input.next();
    }

    private void getCity() {
        System.out.println("city: " + this.city);
    }

    private void setCountry() {
        System.out.print("enter please country: ");
        this.country = this.input.next();
    }

    private void getCountry() {
        System.out.println("country: " + this.country);
    }

    private void setStreet() {
        System.out.print("enter please street: ");
        this.street = this.input.next();
    }

    private void getStreet() {
        System.out.println("street: " + this.street);
    }

    private void setHouse() {
        System.out.print("enter please house: ");
        this.house = this.input.nextInt();
    }

    private void getHouse() {
        System.out.println("house: " + this.house);
    }

    private void setApartment() {
        System.out.print("enter please apartment: ");
        this.apartment = this.input.nextInt();
    }

    private void getApartment() {
        System.out.println("apartment: " + this.apartment);
    }
}
