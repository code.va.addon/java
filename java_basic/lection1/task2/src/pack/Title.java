package java_basic.lection1.task2.src.pack;

import java.util.Scanner;


class Title {
    private String title;

    Title() {
        Scanner input = new Scanner(System.in);

        System.out.print("enter please title book: ");
        this.title = input.nextLine();
    }

    void show() {
        System.out.println("title: " + this.title);
    }
}
