package java_basic.lection1.task2.src.pack;

import java.util.Scanner;

class Content {
    private String content;

    Content() {
        Scanner input = new Scanner(System.in);

        System.out.println("enter please content book:");
        this.content = input.nextLine();
    }

    void show() {
        System.out.println("Description:\n" + this.content);
    }
}
