package java_basic.lection1.task2.src.pack;

import java.util.Scanner;


class Author {
    private String author;

    Author() {
        Scanner input = new Scanner(System.in);

        System.out.print("enter please name author book: ");
        this.author = input.nextLine();
    }

    void show() {
        System.out.println("name author: " + this.author);
    }
}
