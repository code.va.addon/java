package java_basic.lection1.task1.src.pack;


class Rectangle {
    private double side1, side2;

    Rectangle() {}

    double areaCalculator(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
        return this.side1 * this.side2;
    }

    double perimeterCalculator(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
        return 2 * (this.side1 + this.side2);
    }
}
