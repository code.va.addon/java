/* Требуется: Создать класс с именем Rectangle. В теле класса
создать два поля, описывающие длины сторон double side1, double side2. Создать два метода,
вычисляющие площадь прямоугольника – double areaCalculator (double side1, double side2) и периметр
прямоугольника – double perimeterCalculator (double side1, double side2). Написать программу, которая
принимает от пользователя длины двух сторон прямоугольника и выводит на экран периметр и
площадь. */
package java_basic.lection1.task1.src.pack;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        double side1 = 0.0d, side2 = 0.0d;
        Scanner input = new Scanner(System.in);
        Rectangle rectangle = new Rectangle();

        System.out.print("enter please height: ");
        side1 = input.nextDouble();
        System.out.print("enter please lenght: ");
        side2 = input.nextDouble();
        System.out.println("area rectangle: " + rectangle.areaCalculator(side1, side2));
        System.out.println("perimetr rectangle: " + rectangle.perimeterCalculator(side1, side2));
    }
}
