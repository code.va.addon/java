package java_basic.lection1.task3.src.pack.pack;


class Computer {
    private Computer next;

    Computer(Computer next) {
        this.next = next;
    }

    Computer() {}

    void appComuter(int number, Computer[] computers) {
        computers[number] = this;
    }

    Computer getNext() {
        return this.next;
    }
}
