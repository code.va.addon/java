/* Создать класс Computer, создать массив объектов
Computers размером 5. Далее руками создать 5 экземпляров этого класса и записать в компьютер
(используя loop) */
package java_basic.lection1.task3.src.pack.pack;

import java.util.Arrays;


class Main {
    public static void main(String[] args) {
        Computer[] computers = new Computer[5];
        Computer computer1 = new Computer();
        Computer computer2 = new Computer(computer1);
        Computer computer3 = new Computer(computer2);
        Computer computer4 = new Computer(computer3);
        Computer computer5 = new Computer(computer4);
        Computer computer = computer5;

        for (int i = 0; i < computers.length; i++) {
            computer.appComuter(i, computers);
            computer = computer.getNext();
        }
        System.out.println(Arrays.toString(computers));
    }
}
