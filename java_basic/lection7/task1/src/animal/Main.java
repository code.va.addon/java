/* Создайте перечислительный тип (enum) Animals, содержащий конструктор, который должен принимать
целочисленное значение (возраст животного), и содержать перегруженный метод toString(), который
должен возвращать название экземпляра и возраст животного. */
package animal;

enum Animals {
    DOG(6), CAT(4), WOLF(5), ELEFAN(25), FOX(3);

    int value;

    Animals(int val) {
        this.value = val;
    }

    @Override
    public String toString() {
        return "animal " + super.toString() + " have is " + this.value + " year";
    }
}

class Main {
    public static void main(String[] args) {
        Animals[] animals = {Animals.CAT, Animals.DOG, Animals.ELEFAN, Animals.FOX, Animals.WOLF};

        for (Animals animal : animals) {
            System.out.println(animal);
        }
    }
}
