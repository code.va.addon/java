package pack;

class Mobile extends Computer {
    Phone createPhone() {
        return new Phone() {
            @Override
            void processor() {
                super.processor();
                System.out.println("mobile");
            }
        };
    }

    @Override
    void processor() {
        super.processor();
        System.out.println("mobile");
    } 
}
