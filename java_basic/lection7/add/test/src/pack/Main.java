package pack;

class Main {
    public static void main(String[] args) {
        Phone ph1 = new Phone();
        Phone ph2 = new Phone();
        Computer cmp1 = new Computer();
        Computer cmp2 = new Computer();
        Mobile mb1 = new Mobile();
        Mobile mb2 = new Mobile();
        Phone[] phs = {ph1, ph2, mb1.createPhone()};
        Computer[] cmps = {cmp1, cmp2, mb2};

        for (Computer cmp : cmps) {
            cmp.processor();
        }
        for (Phone ph : phs) {
            ph.processor();
        }
    }
}
