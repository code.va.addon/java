/* Создайте перечислительный тип (enum) Vehicles, содержащий конструктор, который должен принимать
целочисленное значение (стоимость автомобиля), содержать метод getColor(), который возвращает
строку с цветом автомобиля, и содержать перегруженный метод toString(), который должен возвращать
строку с названием экземпляра, цветом и стоимостью автомобиля. */
package vehicle;

enum Vehicles {
    PORSHE(50000){
        @Override
        String getColor() {
            return "red";
        }
    }, MERSEDES(45000) {
        @Override
        String getColor() {
            return "black";
        }
    }, DAEWOO(10000) {
        @Override
        String getColor() {
            return "green";
        }
    }, RENO(45000) {
        @Override
        String getColor() {
            return "yelow";
        }
    };

    int price;

    Vehicles (int price) {
        this.price = price;
    }

    String getColor() {
        return "white";
    }

    @Override
    public String toString() {
        return getColor() + " " + super.toString() + " " + price + "$";
    }
}

class Main {
    public static void main(String[] args) {
        Vehicles[] cars = {Vehicles.DAEWOO, Vehicles.MERSEDES, Vehicles.PORSHE, Vehicles.RENO};

        for (Vehicles car : cars) {
            System.out.println(car);
        }
    }
}
