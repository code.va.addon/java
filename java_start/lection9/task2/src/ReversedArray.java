/* Створити метод myReverse(int [] array),
який приймає як аргумент масив цілочислових елементів і повертає інвертований масив
(елементи масиву у зворотному порядку). Створити метод int [] subArray (int [] array, int index, int
count). Метод повертає частину отриманого як аргумент масиву, починаючи з позиції, яка
зазначена в аргументі index, розмірністю, що відповідає значенню аргументу count. Якщо
аргумент count містить значення більше, ніжкількість елементів, що входять до частини вихідного
масиву (від зазначеного індексу index до індексу останнього елемента), то під час формування
нового масиву розмірністю в count, заповніть одиницями ті елементи, які не були скопійовані з
вихідного масиву. */
package java_start.lection9.task2.src;


public class ReversedArray {
    static void myReverse(int[] array) {
        for (byte i = (byte)(array.length - 1); i >= 0; i--) System.out.println(array[i]);
    }

    static int[] subArray(int[] array, int index, int count) {
        int[] newArray = new int[count];

        for (int i = 0; i < count; i++) {
            newArray[i] = (index == array.length) ? 1 : array[index++];
        }
        return newArray;
    }
    public static void main(String[] args) {
        int[] array = {2, 23, 89, 28, 90, 35, 78}, newArray;
        int index = 2, count = 10;

        System.out.println("reverse array");
        myReverse(array);
        System.out.println("");
        System.out.println("new reverse array");
        newArray = subArray(array, index, count);
        myReverse(newArray);
    }
}
