/* Створіть масив розмірністю 10 елементів,
виведіть на екран усі елементи масиву у зворотному порядку. */
package java_start.lection9.add_task1.src;


public class Arrays {
    public static void main(String[] args) {
        byte[] array = new byte[10];

        for (byte i = 0; i < array.length; i++) array[i] = i;
        for (byte i = (byte)(array.length - 1); i >= 0; i--) System.out.println(array[i]);
    }
}
