/* Створити метод, який виконуватиме
збільшення довжини масиву, переданого як аргумент, на 1 елемент. Елементи масиву мають
зберегти своє значення та порядок індексів. Створити метод, який приймає два аргументи,
перший аргумент типу int [] array, другий аргумент типу int value. У тілі методу реалізуйте
можливість додавання другого аргументу методу в масив за індексом 0, водночас довжина
нового масиву має збільшитися на 1 елемент, а елементи одержуваного масиву як перший
аргумент мають скопіюватися в новий масив починаючи з індексу 1. */
package java_start.lection9.task3.src;

import java.util.Arrays;


public class UserArray {
    static int[] addElementArray(int[] array, int value) {
        int[] newArray = new int[array.length + 1];

        for (byte i = 0; i < newArray.length; i++) {
            newArray[i] = (i == 0) ? value : array[i - 1];
        }
        return newArray;
    }
    public static void main(String[] args) {
        int[] array = {4, 5, 6, 7, 8, 9, 0};
        int value = 10;

        array = addElementArray(array, value);
        System.out.println(Arrays.toString(array));
    }
}
