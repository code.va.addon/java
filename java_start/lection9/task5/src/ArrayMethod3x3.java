/* Створіть два масиви 3х3, заповніть їх, створіть третій масив порожній. У третій масив внесіть
результат додавання перших двох, використовуючи методи. */
package java_start.lection9.task5.src;

import java.util.Arrays;

public class ArrayMethod3x3 {
    static int[][] sumArray(int[][] array1, int[][] array2) {
        int[][] newArray = new int[array1.length][array1[0].length];

        for (int i = 0; i < newArray.length; i++) {
            for (int j = 0; j < newArray[i].length; j++) {
                newArray[i][j] = array1[i][j] + array2[i][j];
            }
        }
        return newArray;
    }
    public static void main(String[] args) {
        int[][] array1 = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        },
        array2 = {
            {11, 12, 13},
            {14, 15, 16},
            {17, 18, 19}
        },
        array3 = sumArray(array1, array2);

        for (int i = 0; i < array3.length; i++) {
            System.out.println(Arrays.toString(array3[i]));
        }
    }
}
