/* Створіть два масиви 3х3, заповніть їх, створіть третій масив порожній. У третій масив внесіть
результат додавання перших двох. */
package java_start.lection9.task4.src;

import java.util.Arrays;


public class Array3x3 {
    public static void main(String[] args) {
        int[][] array1 = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        },
        array2 = {
            {11, 12, 13},
            {14, 15, 16},
            {17, 18, 19}
        },
        array3 = new int[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                array3[i][j] = array1[i][j] + array2[i][j];
            }
        }
        for (int i = 0; i < 3; i++) {
            System.out.println(Arrays.toString(array3[i]));
        }
    }
}
