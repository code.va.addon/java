/* Створити масив розміру N-елементів,
заповнити його довільними цілими значеннями (розмір масиву задає користувач). Вивести на
екран: найбільше значення масиву, найменше значення масиву, загальну суму всіх елементів,
середнє арифметичне всіх елементів, вивести всі непарні значення. */
package java_start.lection9.task1.src;

import java.util.Scanner;


public class CustomerArray {
    static void maxMinSumArray(int[] array) {
        int max = array[0], min = array[0], sum = array[0];
        float summ;

        for (int e : array) {
            if (max < e) max = e;
            else if (min > e) min = e;
            sum += e;
        }
        summ = sum / (float)array.length;
        System.out.println("max = " + max);
        System.out.println("min = " + min);
        System.out.println("sum = " + summ);
    }

    static int[] createArray(byte n) {
        int[] array = new int[n];

        for (byte i = 0; i < n; i++) array[i] = i * 2 + 10 - 20;
        return array;
    }
    public static void main(String[] args) {
        byte n;
        int[] array;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please lenght array: ");
        n = input.nextByte();
        array = createArray(n);
        maxMinSumArray(array);
    }
}
