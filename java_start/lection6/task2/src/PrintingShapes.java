/* Використовуючи цикли та метод:
System.out.print("*"), System.out.print(" "), System.out.print("\n") (для переходу на новий рядок).
Виведіть на екран:
• прямокутник;
• прямокутний трикутник;
• рівносторонній трикутник;
• ромб. */


public class PrintingShapes {
    public static void main(String[] args) {
        final byte RECTANGLE1 = 10, RECTANGLE2 = 5; 

        System.out.printf("rectangle %1$spoint x %2$spoint\n", RECTANGLE1, RECTANGLE2);
        for (byte i = 0; i < RECTANGLE2; i++) {
            System.out.print("*");
            System.out.print(" ");
            for (byte j = 0; j < RECTANGLE1 - 2; j++) {
                if (i == 0 || i == 4) {
                    System.out.print("*");
                    System.out.print(" ");
                }
                else {
                    System.out.print(" ");
                    System.out.print(" ");
                }
            }
            System.out.print("*");
            System.out.print("\n");
        }

        System.out.printf("right triangle h = %1$spoint\n", RECTANGLE1);
        for (byte i = 0; i < RECTANGLE1; i++) {
            System.out.print("*");
            for (byte j = 1; j <= i; j++) {
                if ((i == RECTANGLE1 - 1) && ((j & 1) == 0)) System.out.print("*");
                else System.out.print(" ");
            }
            if (i > 0) System.out.print("*");
            System.out.print("\n");
        }

        System.out.printf("equilateral triangle h = %1$spoint\n", RECTANGLE1);
        for (byte i = 0; i < RECTANGLE1; i++) {
            byte k = i;
            if (i > 0) ++k;
            for (byte j = 0; j <= RECTANGLE1 + k; j++) {
                if ((i == RECTANGLE1 - 1) && ((j & 1) == 0)) System.out.print("*");
                else if (j == RECTANGLE1 - k || j == RECTANGLE1 + k) System.out.print("*");
                else System.out.print(" ");
            }
            System.out.print("\n");
        }

        byte heighRhomb = RECTANGLE1 * 2;
        System.out.printf("rhomb h = %1$s\n", heighRhomb);
        for (byte i = 0; i < RECTANGLE1; i++) {
            byte k = i;
            if (i > 0) ++k;
            for (byte j = 0; j <= RECTANGLE1 + k; j++) {
                if (j == RECTANGLE1 - k || j == RECTANGLE1 + k) System.out.print("*");
                else System.out.print(" ");
            }
            System.out.print("\n");
        }
        for (byte i = RECTANGLE1 - 2; i >= 0; i--) {
            byte k = i;
            if (i > 0) ++k;
            for (byte j = 0; j <= RECTANGLE1 + k; j++) {
                if (j == RECTANGLE1 - k || j == RECTANGLE1 + k) System.out.print("*");
                else System.out.print(" ");
            }
            System.out.print("\n");
        }
    }
}
