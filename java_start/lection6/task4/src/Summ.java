/* Дано два цілих числа A і B (A < B). Знайдіть суму всіх цілих чисел від A до B включно.
 */
package java_start.lection6.task4.src;


public class Summ {
    public static void main(String[] args) {
        final int A = 35, B = 245;
        int result = 0;

        for (int i = A; i <= B; i++) result += i;
        System.out.printf("Summa [%1$s < %2$s] = %3$s\n", A, B, result);
    }
}
