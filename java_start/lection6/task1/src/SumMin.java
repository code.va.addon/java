/* Дано два числа A та B (A <B). Виведіть суму
всіх чисел,які розташовані між цими числами на екран. Дано два числа A та B (A <B). Виведіть усі
непарні значення, які розташовані між цими числами. */


public class SumMin {
    public static void main(String[] args) {
        final byte A = 2, B = 35, CTRL_NUMBER = 1;
        byte resultSum = 0;
        String oddNumbers = "";

        for (byte i = A + 1; i < B; i++) {
            resultSum += i;
            if ((i & CTRL_NUMBER) == 1 && i == A + 1) oddNumbers += i;
            else if ((i & CTRL_NUMBER) == 1) oddNumbers += ", " + i;
        }
        System.out.printf("sum (%1$s < %2$s) = %3$s\n", A, B, resultSum);
        System.out.printf("odd numbers (%1$s < %2$s) = [%3$s]\n", A, B, oddNumbers);
    }
}
