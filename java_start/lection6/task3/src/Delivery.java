/* Є N клієнтів, яким компанія-виробник має доставити товар. Скільки є можливих маршрутів
доставленнятовару з урахуванням того, що товар доставлятиме одна машина?
Напишіть програму, яка розраховуватиме та
виводитиме на екран кількість можливих варіантів доставлення товару. Для розв’язку задачі
використовуйте факторіал N!, що розраховується за допомогою циклу do-while. */
import java.util.Scanner;


public class Delivery {
    public static void main(String[] args) {
        int clients, routes, client = 1;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please number of customers: ");
        clients = input.nextInt();
        routes = (clients > 0) ? 1 : 0;
        while (client <= clients) {
            routes *= client++;
        }
        System.out.println("routes = " + routes);
    }
}
