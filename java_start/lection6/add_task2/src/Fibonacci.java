/* Знайдіть послідовність Фібоначчі. Одне стартове число користувач вводить, друге вводить
користувач до шуканого. */
package java_start.lection6.add_task2.src;

import java.util.Scanner;


public class Fibonacci {
    public static void main(String[] args) {
        int f0 = 0, n = 0, i1 = 0, i2 = 0, sum = 0;
        String fibonacci = "";
        Scanner input = new Scanner(System.in);

        System.out.print("enter please f0: ");
        f0 = input.nextInt();
        System.out.print("enter please n: ");
        n = input.nextInt();
        if (n <= 0) return;
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                i1 = f0;
                fibonacci += f0;
                continue;
            }
            if (i == 1 && f0 == 0) i2 = 1;
            else if (i == 1) i2 = f0;
            else if (i >= 2) {
                sum = i1 + i2;
                i1 = i2;
                i2 = sum;
            }
            fibonacci += ", " + i2;
        }
        System.out.printf("[%1$s]\n", fibonacci);
    }
}
