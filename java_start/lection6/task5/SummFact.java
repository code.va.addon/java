/* Дано ціле число N (> 0). Використовуючи один цикл, знайдіть суму 1 + 1 / (1!) + 1 / (2!) + 1 / (3!)
+. . . + 1 /(N!) */
package java_start.lection6.task5;


public class SummFact {
    public static void main(String[] args) {
        final byte N = 10;
        double result = 1.0d;

        for (byte i = 1; i <= N; i++) {
            int fact = 1;
            for (byte j = 1; j <= i; j++) fact *= j;
            result += 1.0d / fact;
        }
        System.out.println("Summa = " + result);
    }
}
