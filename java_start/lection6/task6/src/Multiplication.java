/* Створіть програму таблиці множення для числа 7, використовуючи цикли. Приклад виведення в
консоль:
7 * 1 = 7;
7 * 2 = 14;
...;
7 * 10 = 70. */
package java_start.lection6.task6.src;


public class Multiplication {
    public static void main(String[] args) {
        final byte SEVEN = 7;
        byte result = 0;

        for (byte i = 1; i <= 10; i++) {
            result = (byte)(SEVEN * i);
            System.out.printf("%1$s x %2$s = %3$s;\n", SEVEN, i, result);
        }
    }
}
