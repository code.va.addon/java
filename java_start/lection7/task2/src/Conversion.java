/* Напишіть програму, яка конвертуватиме
валюти.Користувач вводить:
1) суму грошей у певній валюті;
2) курс конвертації в іншу валюту.
Організуйте виведення результату операції конвертування валюти на екран. */
package java_start.lection7.task2.src;

import java.util.Scanner;


public class Conversion {
    static float prompt(String prom) {
        Scanner input = new Scanner(System.in);
        float value = 0.0f;

        System.out.print("enter please " + prom + ": ");
        value = input.nextFloat();
        return value;
    }
    public static void main(String[] args) {
        float money = 0.0f, rate = 0.0f, result = 0.0f;

        money = prompt("money");
        rate = prompt("rate");
        result = money * rate;
        System.out.println(result + "$");
    }
}
