/* Опишіть метод PowerA234(A, B, C, D), який обчислює другий, третій і четвертий ступінь числа A і
повертає ці ступені відповідно до змінних B, C і D (A – вхідний, B, C, D – вихідні параметри; усі
параметри є дійсними). За допомогою цієї процедури знайдіть другий, третій і четвертий ступені
п'яти зазначених чисел. */
package java_start.lection7.task5.src;


public class Power {
    static void Power234(int a, int b, int c, int d) {
        b = a * a;
        c = a * a * a;
        d = a * a * a * a;
        System.out.printf("%1$s ^ 2 = %2$s;\n", a, b);
        System.out.printf("%1$s ^ 3 = %2$s;\n", a, c);
        System.out.printf("%1$s ^ 4 = %2$s;\n", a, d);
        System.out.println("");
    }
    public static void main(String[] args) {
        int A = 0, B = 0, C = 0, D = 0;

        A = 2;
        Power234(A, B, C, D);
        A = 5;
        Power234(A, B, C, D);
        A = 10;
        Power234(A, B, C, D);
        A = 12;
        Power234(A, B, C, D);
        A = 9;
        Power234(A, B, C, D);
    }
}
