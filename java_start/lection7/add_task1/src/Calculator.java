/* Створіть метод з ім'ям calculate, який
приймає якпараметри три цілих аргументи та виводить на екран середнє арифметичне значень
аргументів. */
package java_start.lection7.add_task1.src;

import java.util.Scanner;


public class Calculator {
    static int prompt(Scanner input, String prom) {
        System.out.print("enter please " + prom + ": ");
        return input.nextInt();
    }

    static void calculate(int a, int b, int c) {
        float result = 0.0f;
        
        result = (a + b + c) / 3.0f;
        System.out.printf("(%1$s + %2$s + %3$s) / 3 = %4$s;\n", a, b, c, result);
    }
    public static void main(String[] args) {
        int a = 0, b = 0, c = 0;
        Scanner input = new Scanner(System.in);

        a = prompt(input, "number1");
        b = prompt(input, "number2");
        c = prompt(input, "number3");
        calculate(a, b, c);
    }
}
