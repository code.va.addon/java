/* Опишіть метод PowerA3(A, B), який обчислює третій ступінь числа A і повертає її до змінної B (A –
вхідний, B – вихідний параметр; обидва параметри є дійсними). За допомогою цієї процедури
знайдіть третій ступінь п'яти зазначених чисел. */
package java_start.lection7.task4.src;


public class Power {
    static int PowerA3(int a, int b) {
        b = a * a * a;
        System.out.printf("%1$s ^ 3 = %2$s;\n", a, b);
        return b;
    }
    public static void main(String[] args) {
        int B = 0, A = 0;

        A = 2;
        PowerA3(A, B);
        A = 5;
        PowerA3(A, B);
        A = 10;
        PowerA3(A, B);
        A = 12;
        PowerA3(A, B);
        A = 9;
        PowerA3(A, B);
    }
}
