/* Створіть чотири методи для виконання
арифметичних операцій з іменами: add – додавання, sub – віднімання, mul – множення, div –
ділення. Кожен метод має приймати два цілих аргументи та виводити на екран результат
виконання арифметичної операції відповідної імені методу. Метод поділу div має виконувати
перевірку спроби поділу на нуль.
Потрібно надати користувачеві можливість вводити з клавіатури значення операндів і знак
арифметичної операції для виконання обчислень. */
package java_start.lection7.task1.src;

import java.util.Scanner;


public class Arithmetics {
    static String add(float number1, float number2) {
        float result = number1 + number2;
        return number1 + " + " + number2 + " = " + result;
    }

    static String sub(float number1, float number2) {
        float result = number1 - number2;
        return number1 + " - " + number2 + " = " + result;
    }

    static String mul(float number1, float number2) {
        float result = number1 * number2;
        return number1 + " x " + number2 + " = " + result;
    }

    static String dev(float number1, float number2) {
        String resultStr;
        if (number2 == 0) resultStr = "Error: number2 = 0";
        else {
            float result = number1 / number2;
            resultStr = number1 + " : " + number2 + " = " + result;
        }
        return resultStr;
    }
    public static void main(String[] args) {
        float number1 = 0.0f, number2 = 0.0f;
        String sign, result;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please number1: ");
        number1 = input.nextFloat();
        System.out.print("enter please mathematic sign: ");
        sign = input.next();
        System.out.print("enter please number2: ");
        number2 = input.nextFloat();
        switch (sign) {
            case "+": {
                result = add(number1, number2);
                break;
            }
            case "-": {
                result = sub(number1, number2);
                break;
            }
            case "x": {
                result = mul(number1, number2);
                break;
            }
            case ":": {
                result = dev(number1, number2);
                break;
            }
            default: {
                result = "Error: unknown sign";
            }
        }
        System.out.println(result);
    }
}
