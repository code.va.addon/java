/* Напишіть метод, який визначатиме:
1) чи є введене число позитивним, чи негативним;
2) чи є воно простим (використовуйте техніку перебору значень).
Просте число – це натуральне число, яке ділиться на 1 й саме на себе. Щоби визначити
простечисло чи ні, варто знайти всі його цілі дільники. Якщо дільників більше 2-х, то воно
не просте;
3) чи ділиться воно на 2, 5, 3, 6, 9 без залишку. */
package java_start.lection7.task3.src;

import java.util.Scanner;


public class NumberCheck {
    static void posNeg(int value) {
        String result;

        result = (value >= 0) ? "positive" : "negative";
        System.out.printf("number %1$s is %2$s\n", value, result);
    }

    static void primeNumber(int value) {
        String result = "prime";

        for (byte i = 2; i <= 9; i++) {
            if (value != i && value % i == 0) {
                result = "not prime";
                break;
            }
        }
        System.out.println(value + " is " + result + " number");
    }

    static void dev(int value) {
        String result = " divide ";
        byte ctrlValue = 2;

        while (true) {
            if (value % ctrlValue == 0) {
                result += ctrlValue + " ";
            }
            if (ctrlValue == 2 || ctrlValue == 5) ++ctrlValue;
            else if (ctrlValue == 3) ctrlValue += 2;
            else if (ctrlValue == 6) ctrlValue += 3;
            else break;
        }
        if (result == " divide ") result = " not" + result + "2 5 3 6 9";
        System.out.println(value + result);
    }
    public static void main(String[] args) {
        int number;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please number: ");
        number = input.nextInt();
        posNeg(number);
        primeNumber(number);
        dev(number);
    }
}
