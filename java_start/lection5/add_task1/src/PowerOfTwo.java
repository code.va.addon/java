/* Відомо, що в числах, які є ступенем двійки, лише один біт має значення 1.
Напишіть програму, яка перевірятиме, чи є вказане число ступенем двійки, чи ні. */
package java_start.lection5.add_task1.src;

import java.util.Scanner;


public class PowerOfTwo {
    public static void main(String[] args) {
        int number, result;
        String text;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please number: ");
        number = input.nextInt();
        result = number & (number - 1);
        text = (result == 0) ? " : degree 2" : " : not degree 2";
        System.out.print(number + text);
    }
}
