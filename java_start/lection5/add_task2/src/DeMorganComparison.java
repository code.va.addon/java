/* Використовуючи теорему де Моргана, перетворіть вихідний вираз A | B на еквівалентний вираз. */
package java_start.lection5.add_task2.src;


public class DeMorganComparison {
    public static void main(String[] args) {
        boolean a = true, b = false;
        String text;

        text = (!(!a && !b)) ? "true" : "false";
        System.out.print(text);        
    }
}
