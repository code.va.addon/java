/* Напишіть програму розрахунку нарахування премійпрацівникам. Премії розраховуються згідно з вислугою років. Якщо вислуга
до 5 років, премія становить 10% від заробітної плати. Якщо вислуга від 5 років (включно) до 10 років, то премія становить 15% від заробітної плати. Якщо вислуга від 10 років (включно) до 15 років, премія становить 25% від
заробітної плати. Якщо вислуга від 15 років (включно) до 20 років, премія становить 35% від заробітної плати. Якщовислуга від 20 років (включно) до 25 років, премія становить 45% від заробітної
плати. Якщо вислуга від 25 років (включно) та більше, премія складає 50% від заробітної плати. Результати розрахунку виведіть на екран. */
package java_start.lection5.task3.src;

import java.util.Scanner;


public class Premium {
    public static void main(String[] args) {
        float wage = 0.0f, bonus = 0.0f;
        byte experience = 0;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please experience: ");
        experience = input.nextByte();
        System.out.print("enter please wage: ");
        wage = input.nextFloat();

        if (experience >= 5 && experience < 10) bonus = wage * 0.15f;
        else if (experience >= 10 && experience < 15) bonus = wage * 0.25f;
        else if (experience >= 15 && experience < 20) bonus = wage * 0.35f;
        else if (experience >= 20 && experience < 25) bonus = wage * 0.45f;
        else if (experience >= 25) bonus = wage * 0.5f;
        else if (experience < 0 || wage < 0) {
            System.out.print("Error: not correct experience or wage");
            return;
        }

        System.out.print("premium = " + bonus);
    }
}
