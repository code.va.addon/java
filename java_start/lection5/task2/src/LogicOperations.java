/* Є 3 змінні типу int x = 5, y = 10, і z = 15;
Виконайте та розрахуйте результат таких операцій для цих змінних:
x += y >> x++ * z;
z = ++x & y * 5;
y /= x + 5 | z;
z = x++ & y * 5;
x = y << x++ ^ z; */
package java_start.lection5.task2.src;


public class LogicOperations {
    public static void main(String[] args) {
        int x = 5, y = 10, z = 15;

        x += y >> x++ * z;
        System.out.println("x += y >> x++ * z; x = " + x);

        z = ++x & y * 5;
        System.out.println("z = ++x & y * 5; z = " + z);

        y /= x + 5 | z;
        System.out.println("y /= x + 5 | z; y = " + y);

        z = x++ & y * 5;
        System.out.println("z = x++ & y * 5; z = " + z);

        x = y << x++ ^ z;
        System.out.print("x = y << x++ ^ z; x = " + x);
    }
}
