/* Відомо, що в парних числах молодший біт має значення 0. Напишіть програму, яка виконуватиме перевірку чисел на парність. Запропонуйте
два варіанти розв'язку поставленого завдання. */
import java.util.Scanner;


public class Parity {
    public static void main(String[] args) {
        int number, result;
        final int CTRL_NUMBER = 1;
        String resulText;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please number: ");
        number = input.nextInt();
        result = number & CTRL_NUMBER;
        resulText = (result == 0) ? "even number" : "odd number";
        System.out.println("variable 1: " + number + " it is " + resulText);

        result = ~(~number | ~CTRL_NUMBER);
        resulText = (result == 0) ? "even number" : "odd number";
        System.out.println("variable 2: " + number + " it is " + resulText);
    }
}
