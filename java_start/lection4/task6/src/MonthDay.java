/* Дано номер місяця – ціле число в діапазоні 1–12 (1 – січень, 2 – лютий і так далі). Визначте
кількість днів цього місяця для невисокосного року. */
package java_start.lection4.task6.src;

import java.util.Scanner;


public class MonthDay {
    public static void main(String[] args) {
        byte day = 0, month = 0;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please number month: ");
        month = (byte)input.nextInt();
        switch (month) {
            case 2: {
                day = 28;
                break;
            }
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12: {
                day = 31;
                break;
            }
            case 4:
            case 6:
            case 9:
            case 11: {
                day = 30;
                break;
            }
            default: {
                System.out.print("Error: unknown number month: " + month);
                return;
            }
        }
        System.out.printf("month number %1$s have %2$s days", month, day);
    }
}
