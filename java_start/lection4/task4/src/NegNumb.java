/* Дано ціле число. Якщо воно є позитивним, додайте до нього 1; інакше відніміть із нього 2.
Виведіть отримане число. */
package java_start.lection4.task4.src;

import java.util.Scanner;


public class NegNumb {
    public static void main(String[] args) {
        int number;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please number: ");
        number = input.nextInt();

        if (number >= 0) ++number;
        else number -= 2;

        System.out.print(number);
    }
}
