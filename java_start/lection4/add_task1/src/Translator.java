/* Напишіть програму «Українсько-англійський перекладач». Програма знає 10 слів про погоду.
Потрібно, щоби користувач вводив слово українською мовою, а програма давала йому
переклад англійською мовою. Якщо користувач ввів слово, для якого немає перекладу, варто
вивести повідомлення, що такого слова немає. */
package java_start.lection4.add_task1.src;

import java.util.Scanner;


public class Translator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String word, result = "";
       
        System.out.print("enter please word: ");
        word = input.next();
        switch (word) {
            case "дощь": {
                result = "rain";
                break;
            }
            case "сонце": {
                result = "sun";
                break;
            }
            case "сніг": {
                result = "snow";
                break;
            }
            case "туман": {
                result = "fog";
                break;
            }
            case "вітер": {
                result = "snow";
                break;
            }
            case "буря": {
                result = "storm";
                break;
            }
            case "град": {
                result = "hail";
                break;
            }
            case "ожеледиця": {
                result = "icicle";
                break;
            }
            case "пахмурно": {
                result = "cloudy";
                break;
            }
            case "спека": {
                result = "heat";
                break;
            }
            default: {
                System.out.println("Error: not known word");
                return;
            }
        }
        System.out.printf("ua:%1$s -> en:%2$s\n", word, result);
    }
}
