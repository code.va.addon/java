/* Дано ціле число. Якщо воно є позитивним, додайте до нього 1; інакше не змінюйте його.
Виведіть отримане число. */
package java_start.lection4.task3.src;

import java.util.Scanner;


public class PosNumb {
    public static void main(String[] args) {
        int number;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please number: ");
        number = input.nextInt();
        if (number >= 0) ++number;
        System.out.print(number);
    }
}
