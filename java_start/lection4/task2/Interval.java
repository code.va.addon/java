/* Напишіть програму визначення: чи потрапляє вказане користувачем число від 0 до 100 до
числового проміжку [0 – 14] [15 – 35] [36 – 49][50 – 100]. Якщо так, то вкажіть, який саме
проміжок. Якщо користувач вказує число, яке не входить до жодного з наявних числових
проміжків, то виведіть відповідне повідомлення. */
package java_start.lection4.task2;

import java.util.Scanner;


public class Interval {
    public static void main(String[] args) {
        byte number = -1;
        String result = "";
        Scanner input = new Scanner(System.in);

        System.out.print("input please number 0-100: ");
        number = (byte)input.nextInt();
        if (number <= 14 && number >= 0) result = "[0-14]";
        else if (number <= 35 && number >= 15) result = "[15-35]";
        else if (number <= 49 && number >= 36) result = "[36-50]";
        else if (number <= 100 && number >= 50) result = "[50-100]";
        else {
            System.out.print("Error: not a valid number " + number);
            return;
        }
        System.out.print("number in numbers space " + result);
    }
}
