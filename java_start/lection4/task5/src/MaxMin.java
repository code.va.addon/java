/* Дано три цілих числа. Знайдіть максимальне, мінімальне та середнє.
 */
package java_start.lection4.task5.src;

import java.util.Scanner;


public class MaxMin {
    public static void main(String[] args) {
        int number1, number2, number3, max, min;
        float average;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please number: ");
        number1 = input.nextInt();
        System.out.print("enter please number: ");
        number2 = input.nextInt();
        System.out.print("enter please number: ");
        number3 = input.nextInt();

        max = (number1 > number2) ? number1 : number2;
        if (number3 > max) max = number3;
        min = (number1 < number2) ? number1 : number2;
        if (number3 < min) min = number3;
        average = (number1 + number2 + number3) / 3;

        System.out.println("max number: " + max);
        System.out.println("min number: " + min);
        System.out.print("average number: " + average);
    }
}
