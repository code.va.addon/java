/* Напишіть програму «Консольний калькулятор».
Створіть дві змінні з іменами operand1 та operand2. Задайте змінним деякі довільні значення.
Запропонуйте користувачу ввести знак арифметичної операції. Візьміть значення, введене
користувачем,і помістіть його в рядкову змінну sign.
Для організації вибору алгоритму обчислювального процесу використовуйте перемикач switch.
Виведіть на екран результат виконання арифметичної операції.
У разі використання операції розподілу організуйте перевірку спроби розподілу на нуль. І якщо
така є, то скасуйте виконання арифметичної операції та повідомите користувача про помилку. */
import java.util.Scanner;


public class Calculator {
    public static void main(String[] args) {
        float operand1 = 71.32f, operand2 = 17.02f, result = 0.0f;
        Scanner input = new Scanner(System.in);
        String sign = "";

        System.out.print("please input a mathematical symbol: ");
        sign = input.next();

        switch (sign) {
            case "+": {
                result = operand1 + operand2;
                break;
            }
            case "-": {
                result = operand1 - operand2;
                break;
            }
            case "x": {
                result = operand1 * operand2;
                break;
            }
            case ":": {
                if (operand2 != 0.0f) result = operand1 / operand2;
                break;
            }
            default: {
                System.out.print("Error: unlnknown symbol");
                return;
            }
        }
        if (result == 0.0f) {
            System.out.printf(
                "Error: math operation %1$s : %2$s, operand2 = %2$s", 
                operand1, 
                operand2
            );
        }
        else {
            System.out.printf(
                "%1$s %2$s %3$s = %4$s", 
                operand1, 
                sign, 
                operand2, 
                result
            );
        }
    }
}
