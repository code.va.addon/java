/* Напишіть програму розрахунку об'єму – V та площі поверхні -S циліндра.
Об'єм V циліндра радіусом – R та висотою – h, вираховується за формулою: V
= πR2hПлоща поверхні циліндра вираховується за формулою: S = 2πR2 +
2πRh = 2πR(R+h)
Результати розрахунків виведіть на екран. */
package java_start.lection3.task4;

import java.util.Scanner;
import static java.lang.Math.pow;


public class Volume {
    public static void main(String[] args) {
        float v = 0.0f, s = 0.0f, r = 0.0f, h = 0.0f, areaCircle = 0.0f;
        final float PI = 3.1415f;
        Scanner input = new Scanner(System.in);

        System.out.print("input please radius: ");
        r = input.nextFloat();
        System.out.print("input please height: ");
        h = input.nextFloat();
        areaCircle = PI * (float)pow(r, 2);
        v = areaCircle * h;
        s = 2 * areaCircle + 2 * PI * r * h;
        System.out.println("volume of a cylinder: " + v);
        System.out.print("area of a cylinder: " + s);
    }
}
