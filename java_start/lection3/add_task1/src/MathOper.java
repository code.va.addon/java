/* Створіть дві цілочисельні змінні та виведіть на екран результати всіх арифметичних операцій над
цимидвома змінними. */
package java_start.lection3.add_task1.src;


public class MathOper {
    public static void main(String[] args) {
       int property1 = 345, property2 = 35, result = 0;
       
       System.out.printf("property1 = %1$s, property2 = %2$s\n", property1, property2);
       result = property1 + property2;
       System.out.println("property1 + property2 = " + result);
       result = property1 - property2;
       System.out.println("property1 - property2 = " + result);
       result = property1 * property2;
       System.out.println("property1 x property2 = " + result);
       result = property1 / property2;
       System.out.println("property1 : property2 = " + result);
       result = property1 % property2;
       System.out.println("property1 % property2 = " + result);
    }
}
