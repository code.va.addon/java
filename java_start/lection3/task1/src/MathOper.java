/* Є 3 змінні типу int x = 10, y = 12 та z = 3. Виконайте розрахунок результату таких операцій для цих
змінних:
 x += y – x++ * z;
 z = --x – y * 5;
 y /= x + 5 % z;
 z = x++ + y * 5;
 x = y – x++ * z; */



 public class MathOper {
    public static void main(String[] args) {
        int x = 10, y = 12, z = 3;

        x += y - x++ * z;
        System.out.println("value x = " + x);

        z = --x - y * 5;
        System.out.println("value z = " + z);

        System.out.println("value x = " + x); // -9
        int t = 5 % z;
        System.out.println("value 5 % z = " + t); // 5
        y /= x + 5 % z;
        System.out.println("value y = " + y);

        z = x++ + y * 5;
        System.out.println("value z = " + z);

        x = y - x++ * z;
        System.out.print("value x = " + x);
    }
 }
