/* Перевірте, чи можна створити змінні з такими іменами:
uberflu?, _Identifier, \u006fIdentifier, &myVar, myVariab1le */
package java_start.lection3.task5;

public class Main {
    public static void main(String[] args) {
        //byte uberflu? = 0; //error
        byte _Identifier = 0;
        byte \u006fIdentifier = 0;
        //byte &myVar = 0; //error
        byte myVariab1le = 0;
    }
}
