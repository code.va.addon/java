/* Створіть константу під назвою PI (число π «пі»), створіть змінну радіус із назвою – r.
Використовуючиформулу πR2, обчисліть площу кола та виведіть результат на екран. */
import java.util.Scanner;
import static java.lang.Math.pow;


public class Circle {
    public static void main(String[] args) {
        final float PI = 3.1415f;
        float r = 0.0f, result = 0.0f;
        Scanner input = new Scanner(System.in);

        System.out.print("input please radius: ");
        r = input.nextFloat();
        result = PI * (float)pow(r, 2);
        System.out.print("area of a circle = " + result);
    }
}
