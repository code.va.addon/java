/* Вирахуйте середнє арифметичне трьох цілих значень і виведіть його на екран.
З якою проблемою ви зіткнулися? Який тип змінних краще використовуватиме для коректного
показу результату? */



public class ArtmAvrg {
    public static void main(String[] args) {
        //int x = 10, y = 6, z = 12;
        //int result = (x + y + z) / 3;
        float x = 10, y = 6, z = 12;
        float result = (x + y + z) / 3;

        System.out.print(result);
    }
}
