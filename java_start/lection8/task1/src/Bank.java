/* Уявіть, що ви реалізуєте програму для банку, яка допомагає визначити, погасив клієнт кредит чи
ні. Припустимо, щомісячна сума платежу має становити 100 грн. Клієнт має виконати 7 платежів,
але може платити рідше великими сумами. Тобто може двома платежами по 300 і 400 грн
закрити весь борг.
Створіть метод, який як аргумент прийматиме суму платежу, введену економістом банку. Метод
виводить на екран інформацію про стан кредиту (сума заборгованості, сума переплат,
повідомлення провідсутність боргу). */
import java.util.Scanner;


public class Bank {
    static void credit(float contribution) {
        float credit = 700.0f, part = credit / 7, amountOwed = 0.0f;
        String message = "there is no debt";
        float overpayments = (contribution > part) ? contribution - part : 0.0f;

        if (contribution < part) {
            amountOwed = part - contribution;
            message = "ATENTION: there is a debt";
        }
        System.out.println("amount owed: " + amountOwed);
        System.out.println("overpayment: " + overpayments);
        System.out.println(message);
    }
    public static void main(String[] args) {
        float contribution;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please payment: ");
        contribution = input.nextFloat();
        credit(contribution);
    }
}
