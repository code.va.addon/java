/* Створіть три перевантажені методи для пошуку середнього арифметичного числа з 3, 4, 5
змінних, створіть метод для пошуку мінімального, максимального та середнього значень серед
цих трьох методів. */
package java_start.lection8.task3.src;

import java.util.Scanner;


public class Arithmetick {
    static float math(byte a, byte b, byte c) {
        float mat;

        mat = (a + b + c) / 3.0f;
        System.out.printf("(%1$s + %2$s + %3$s) : 3 = %4$s;\n", a, b, c, mat);
        return mat;
    }

    static float math(byte a, byte b, byte c, byte d) {
        float mat ;

        mat = (a + b + c + d) / 4.0f;
        System.out.printf("(%1$s + %2$s + %3$s + %4$s) : 4 = %5$s;\n", a, b, c, d, mat);
        return mat;
    }

    static float math(byte a, byte b, byte c, byte d, byte f) {
        float mat;

        mat = (a + b + c + d + f) / 5.0f;
        System.out.printf("(%1$s + %2$s + %3$s + %4$s + %5$s) : 5 = %6$s;\n", a, b, c, d, f, mat);
        return mat;
    }

    static void math(float a, float b, float c) {
        float min, max, mat;

        if (a > b) {
            min = b;
            max = a;
        }
        else {
            min = a;
            max = b;
        }
        if (min > c) min = c;
        else if (max < c) max = c;
        mat = (a + b + c) / 3;
        System.out.println("min = " + min);
        System.out.println("max = " + max);
        System.out.println("arithmetic mean = " + mat);
    }

    static byte prompt(Scanner input) {
        byte var;

        System.out.print("enter please number: ");
        var = input.nextByte();
        return var;
    }
    public static void main(String[] args) {
        byte a , b, c, d, f;
        float mat1, mat2, mat3;
        Scanner input = new Scanner(System.in);

        a = prompt(input);
        b = prompt(input);
        c = prompt(input);
        d = prompt(input);
        f = prompt(input);
        mat1 = math(a, b, c);
        mat2 = math(a, b, c, d);
        mat3 = math(a, b, c, d, f);
        math(mat1, mat2, mat3);
    }
}
