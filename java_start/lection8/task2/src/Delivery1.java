/* Є N клієнтів, яким компанія-виробник має доставити товар. Скільки є можливих маршрутів
доставлення товару з урахуванням того, що товар доставлятиме одна машина?
Напишіть програму, яка розраховуватиме та
виводитиме на екран кількість можливих варіантів доставлення товару. Для розв'язку задачі,
використовуйте факторіал N!, що розраховується рекурсією. Поясніть, чому не рекомендується
використовувати рекурсію для розрахунку факторіала. Вкажіть слабкі місця цього підходу. */
import java.util.Scanner;


public class Delivery1 {
    static byte route(byte clients, byte routes, byte client) {
        if (client <= clients) {
            routes *= client++;
            routes = route(clients, routes, client);
        }
        return routes;
    }
    public static void main(String[] args) {
        byte clients, routes, client = 1;
        Scanner input = new Scanner(System.in);

        System.out.print("enter please customers: ");
        clients = input.nextByte();
        routes = (clients > 0) ? (byte)1 : (byte)0;
        System.out.println("routes = " + route(clients, routes, client));
    }
}
