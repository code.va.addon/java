/* Створіть метод з ім'ям calculate, який приймає як параметри три цілочислові значення та
повертаєзначення кожного аргументу, поділеного на 5. */
package java_start.lection8.add_task1.src;

import java.util.Scanner;


public class Calculator {
    static void calculate(int a, int b, int c) {
        float result;
        
        if (a != 0) {
            result = a / 5.0f;
            System.out.printf("%1$s / 5 = %2$s;\n", a, result);
            a = 0;
            calculate(b, c, a);
        }
    }

    static int prompt(Scanner input) {
        System.out.print("enter please number: ");
        return input.nextInt();
    }
    public static void main(String[] args) {
        int a, b, c;
        Scanner input = new Scanner(System.in);

        a = prompt(input);
        b = prompt(input);
        c = prompt(input);
        calculate(a, b, c);
    }
}
