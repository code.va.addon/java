/* Дано значення числа pi, яке дорівнює 3,141592653 та значення числа Ейлера е, яке дорівнює
2,7182818284590452. Створіть дві змінні, присвойте їм значення числа pi та числа е та виведіть їх
наекран без втрати точності. */



public class PiEl {
   public static void main(String[] args) {
        double pi = 3.141592653d;
        String el = "2.7182818284590452";

        System.out.println(pi);
        System.out.print(el);
   } 
}
