/* Створіть необхідну кількість змінних типу char,
кожній
змінній надайте значення одного символу у форматі UNICODE. Виведіть у консоль фразу
«Добридень,ВАШЕ_ІМ'Я!». */
package java_start.lection2.add_task1.src;


public class Unicode {
    public static void main(String[] args) {
       char d = '\u0414';
       char o = '\u043E';
       char b = '\u0431';
       char r = '\u0440';
       char y = '\u0438';
       char j = '\u0439';
       char sp = '\u0020';
       char e = '\u0435';
       char n = '\u043D';
       char q = '\u044C';
       char pnc = '\u002C';
       char m = '\u041C';
       char a = '\u0430';
       char k = '\u043A';
       char s = '\u0441';
       char ou = '\u0021';
       
       System.out.print(d);
       System.out.print(o);
       System.out.print(b);
       System.out.print(r);
       System.out.print(y);
       System.out.print(j);
       System.out.print(sp);
       System.out.print(d);
       System.out.print(e);
       System.out.print(n);
       System.out.print(q);
       System.out.print(pnc);
       System.out.print(sp);
       System.out.print(m);
       System.out.print(a);
       System.out.print(k);
       System.out.print(s);
       System.out.println(ou);
    }
}
