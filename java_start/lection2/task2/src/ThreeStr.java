/* Створіть три рядкові змінні та надайте їм
значення:
"\nмій рядок 1"
"\tмій рядок 2"
"\aмій рядок 3"
Виведіть значення кожної змінної на екран. Які відмінності ви побачили? Зробіть висновки. */



public class ThreeStr {
   public static void main(String[] args) {
        String column_1 = "\nмій рядок 1";
        String column_2 = "\tмій рядок 2";
        String column_3 = "\rмій рядок 3";

        System.out.println(column_1);
        System.out.println(column_2);
        System.out.print(column_3);
   } 
}
