/* Створіть проект. Створіть змінну типу byte, надайте їй значення 99999, виведіть значення цієї
змінної в консоль. За виникнення помилки, використовуючи коментарі, напишіть, у чому
помилка, і привласніть це значення у відповідний тип даних. */



public class TypeError {
   public static void main(String[] args) {
        //byte number = 99999;
        //max value type byte have is 127
        //Type mismatch: cannot convert from int to byte
        int number = 99999;

        System.out.print(number);
   } 
}
